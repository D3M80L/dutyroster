﻿namespace DutyRoster.BLL.Infrastructure
{
    public class Command
    {
    }

    public class Command<TResult> : Command
        where TResult: class
    {
        public TResult Result { get; set; }
    }
}
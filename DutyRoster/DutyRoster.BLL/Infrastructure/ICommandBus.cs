﻿namespace DutyRoster.BLL.Infrastructure
{
    public interface ICommandBus
    {
        TCommand Execute<TCommand>(TCommand command)
            where TCommand : Command;
    }
}
﻿using Autofac;
using DutyRoster.BLL.Infrastructure;

namespace DutyRoster.BLL.Implementations
{
    internal sealed class CommandBus : ICommandBus
    {
        private readonly IComponentContext componentContext;

        public CommandBus(IComponentContext componentContext)
        {
            this.componentContext = componentContext;
        }

        public TCommand Execute<TCommand>(TCommand command) 
            where TCommand : Command
        {
            var commandHandler = componentContext.Resolve<ICommandHandler<TCommand>>();
            commandHandler.Handle(command);
            return command;
        }
    }
}

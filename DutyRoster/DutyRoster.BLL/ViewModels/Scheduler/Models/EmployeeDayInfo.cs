﻿using System;

namespace DutyRoster.BLL.ViewModels.Scheduler.Models
{
    public sealed class EmployeeDayInfo
    {
        public int Day { get; set; }

        public DayType DayType { get; set; }

        public bool IsFixed { get; set; }

        public bool HasLeave { get; set; }

        public TimeSpan Start { get; set; }

        public TimeSpan End { get; set; }

        public bool HasNote { get; set; }
    }
}
﻿using System.Collections.Generic;
using DutyRoster.BLL.Models;

namespace DutyRoster.BLL.ViewModels.Scheduler.Models
{
    public sealed class MonthInfo
    {
        public string Title { get; set; }
        public int Year { get; set; }

        public int Month { get; set; }

        public int WorkingDays { get; set; }

        public int TotalDays { get; set; }

        public IEnumerable<MonthWorkHours> WorkHours { get; set; }
    }
}
using System;

namespace DutyRoster.BLL.ViewModels.Scheduler.Models
{
    public sealed class MonthDay
    {
        public int Day { get; set; }

        public bool IsHoliday { get; set; }

        public DayOfWeek DayOfWeek { get; set; }

        public string Note { get; internal set; }
    }
}
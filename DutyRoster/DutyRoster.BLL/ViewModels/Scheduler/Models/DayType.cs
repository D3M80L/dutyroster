﻿namespace DutyRoster.BLL.ViewModels.Scheduler.Models
{
    public enum DayType
    {
        Undefined = 0,
        DayShift = 1,
        NightShift = 2,
        FullDay = 3,
        Holiday = 4,
        Custom = 5
    }
}
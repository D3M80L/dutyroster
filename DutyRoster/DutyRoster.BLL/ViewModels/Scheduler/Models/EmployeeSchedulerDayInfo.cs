﻿using System;

namespace DutyRoster.BLL.ViewModels.Scheduler.Models
{
    public sealed class EmployeeSchedulerDayInfo
    {
        public int EmployeeId { get; set; }

        public int Year { get; set; }

        public int Month { get; set; }

        public DayType DayType { get; set; }
        public int Day { get; set; }

        public TimeSpan Start { get; set; }

        public TimeSpan End { get; set; }
    }
}

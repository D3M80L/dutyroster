﻿using System.Collections.Generic;
using DutyRoster.BLL.DataContracts;

namespace DutyRoster.BLL.ViewModels.Scheduler.Models
{
    public sealed class EmployeeMonthInfo : IEmployeeId
    {
        public IEnumerable<EmployeeDayInfo> Days { get; set; }

        public int EmployeeId { get; set; }

        public string Initials { get; set; }

        public int MinutesToWork { get; set; }
    }
}
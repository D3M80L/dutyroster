using System.Collections.Generic;
using DutyRoster.BLL.ViewModels.Scheduler.Models;

namespace DutyRoster.BLL.ViewModels.Scheduler
{
    public class MonthScheduler
    {
        public int TotalDays { get; set; }

        public IEnumerable<MonthDay> Days { get; set; }

        public string Title { get; set; }

        public IEnumerable<EmployeeMonthInfo> Employees { get; set; }

        public int Year { get; set; }

        public int Month { get; set; }
        public bool IsLocked { get; set; }
    }
}
namespace DutyRoster.BLL.DataContracts
{
    public interface IEmployeeId
    {
        int EmployeeId { get; set; }
    }
}
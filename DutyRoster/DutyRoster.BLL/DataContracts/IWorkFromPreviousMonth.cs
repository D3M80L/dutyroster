namespace DutyRoster.BLL.DataContracts
{
    public interface IWorkFromPreviousMonth
    {
        int WorkFromPreviousMonth { get; set; }
    }
}
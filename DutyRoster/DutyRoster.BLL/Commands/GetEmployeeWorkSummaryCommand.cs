﻿using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.Models.Employee;

namespace DutyRoster.BLL.Commands
{
    public sealed class GetEmployeeWorkSummaryCommand : Command<EmployeeWorkSummary>
    {
        public GetEmployeeWorkSummaryCommand(int employeeId, int year, int month)
        {
            EmployeeId = employeeId;
            Year = year;
            Month = month;
        }

        public int EmployeeId { get; }

        public int Year { get; }

        public int Month { get; }
    }
}
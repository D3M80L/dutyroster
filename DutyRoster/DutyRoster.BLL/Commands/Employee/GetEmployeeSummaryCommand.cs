﻿using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.Models.Employee;

namespace DutyRoster.BLL.Commands.Employee
{
    public sealed class GetEmployeeSummaryCommand : Command<EmployeeSummaryViewModel>
    {
        public GetEmployeeSummaryCommand(int employeeId)
        {
            EmployeeId = employeeId;
        }

        public int EmployeeId { get; }
    }
}

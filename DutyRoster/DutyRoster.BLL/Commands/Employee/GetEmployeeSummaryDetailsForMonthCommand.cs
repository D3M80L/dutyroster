﻿using System;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.Models.Employee;

namespace DutyRoster.BLL.Commands.Employee
{
    public sealed class GetEmployeeSummaryDetailsForMonthCommand : Command<EmployeeSummaryMonthViewModel>
    {
        public GetEmployeeSummaryDetailsForMonthCommand(int employeeId, DateTime date)
        {
            EmployeeId = employeeId;
            Date = date.Date;
        }

        public int EmployeeId { get; }

        public DateTime Date { get; }
    }
}
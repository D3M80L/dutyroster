﻿using System.Collections.Generic;
using DutyRoster.BLL.Infrastructure;

namespace DutyRoster.BLL.Commands.Employee
{
    public sealed class GetEmployeeListViewModelCommand : Command<EmployeeListViewModel>
    {
    }

    public sealed class EmployeeListViewModel
    {
        public IEnumerable<EmployeeListItem> Items { get; set; }
    }

    public sealed class EmployeeListItem
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public string Initials { get; set; }

        public bool IsActive { get; set; }
    }
}

﻿using System;

namespace DutyRoster.BLL.Commands.Employee.Models
{
    public sealed class EmployeeViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Initials { get; set; }

        public DateTime Start { get; set; }

        public DateTime? End { get; set; }

        public int EmploymentType { get; set; }

        public int? MinumumWorkInMonthInMinutes { get; set; }
    }
}
using DutyRoster.BLL.Infrastructure;
using DutyRoster.DAL.Commands;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models;
using DutyRoster.DAL.Models.Employees;

namespace DutyRoster.BLL.Commands.Employee.CommandHandlers
{
    internal sealed class CreateEmployeeViewModelCommandHandler : ICommandHandler<CreateEmployeeViewModelCommand>
    {
        private readonly IDatabaseCommand databaseCommand;
        private readonly IUnitOfWork unitOfWork;

        public CreateEmployeeViewModelCommandHandler(IDatabaseCommand databaseCommand, IUnitOfWork unitOfWork)
        {
            this.databaseCommand = databaseCommand;
            this.unitOfWork = unitOfWork;
        }

        public void Handle(CreateEmployeeViewModelCommand command)
        {
            var employee = new DAL.Models.Employees.Employee();
            employee.FirstName = command.Model.FirstName;
            employee.LastName = command.Model.LastName;
            employee.Initials = command.Model.Initials;
            employee.WorkPeriod = new EmployeeWorkPeriod();
            employee.WorkPeriod.Start = command.Model.Start;
            employee.WorkPeriod.End = command.Model.End;
            employee.WorkPeriod.EmploymentType = (EmploymentType)command.Model.EmploymentType;
            employee.WorkPeriod.MinumumWorkPerMonthInMinutes = command.Model.MinumumWorkInMonthInMinutes;

            databaseCommand.ExecuteCommand(new AddEmployeeDataCommand(employee));

            unitOfWork.Save();
        }
    }
}
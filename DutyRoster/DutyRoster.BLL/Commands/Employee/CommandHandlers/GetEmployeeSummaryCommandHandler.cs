﻿using System;
using System.Linq;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.Models.Employee;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Queries.Employees;

namespace DutyRoster.BLL.Commands.Employee.CommandHandlers
{
    internal sealed class GetEmployeeSummaryCommandHandler : ICommandHandler<GetEmployeeSummaryCommand>
    {
        private readonly ICommandBus commandBus;
        private readonly IDatabaseQuery databaseQuery;

        public GetEmployeeSummaryCommandHandler(IDatabaseQuery databaseQuery, ICommandBus commandBus)
        {
            this.databaseQuery = databaseQuery;
            this.commandBus = commandBus;
        }

        public void Handle(GetEmployeeSummaryCommand command)
        {
            var employee = databaseQuery
                .ExecuteQuery(new GetEmployee(command.EmployeeId));

            var workPeriod = databaseQuery
                .ExecuteQuery(new GetEmployeeWorkPeriodsInMonth(command.EmployeeId, DateTime.Now))
                .SingleOrDefault();

            var result = new EmployeeSummaryViewModel();
            result.Id = command.EmployeeId;
            result.FullName = employee.FirstName + " " + employee.LastName;
            result.Initials = employee.Initials;
            result.WorkType = workPeriod.EmploymentType.ToString();

            result.MonthSummary = commandBus.Execute(new GetEmployeeSummaryDetailsForMonthCommand(command.EmployeeId, DateTime.Now)).Result;
            command.Result = result;
        }
    }
}
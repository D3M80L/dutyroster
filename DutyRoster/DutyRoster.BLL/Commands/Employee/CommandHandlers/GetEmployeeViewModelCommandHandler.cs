﻿using DutyRoster.BLL.Commands.Employee.Models;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Queries.Employees;

namespace DutyRoster.BLL.Commands.Employee.CommandHandlers
{
    internal sealed class GetEmployeeViewModelCommandHandler : ICommandHandler<GetEmployeeViewModelCommand>
    {
        private readonly IDatabaseQuery databaseQuery;

        public GetEmployeeViewModelCommandHandler(IDatabaseQuery databaseQuery)
        {
            this.databaseQuery = databaseQuery;
        }

        public void Handle(GetEmployeeViewModelCommand command)
        {
            var viewModel = new EmployeeViewModel();
            command.Result = viewModel;

            var employee = databaseQuery.ExecuteQuery(new GetEmployee(command.Id));
            viewModel.FirstName = employee.FirstName;
            viewModel.LastName = employee.LastName;
            viewModel.Initials = employee.Initials;
            viewModel.Start = employee.WorkPeriod.Start;
            viewModel.End = employee.WorkPeriod.End;
            viewModel.EmploymentType = (int) employee.WorkPeriod.EmploymentType;
            viewModel.MinumumWorkInMonthInMinutes = employee.WorkPeriod.MinumumWorkPerMonthInMinutes;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Globalization;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.Models.Employee;

namespace DutyRoster.BLL.Commands.Employee.CommandHandlers
{
    internal sealed class GetEmployeeSummaryDetailsForMonthCommandHandler : ICommandHandler<GetEmployeeSummaryDetailsForMonthCommand>
    {
        private readonly ICommandBus commandBus;

        public GetEmployeeSummaryDetailsForMonthCommandHandler(ICommandBus commandBus)
        {
            this.commandBus = commandBus;
        }

        public void Handle(GetEmployeeSummaryDetailsForMonthCommand command)
        {
            var result = new EmployeeSummaryMonthViewModel();
            result.Date = command.Date;
            var items = new List<EmployeeSummaryItem>();

            var employeeWorkSummary = commandBus.Execute(new GetEmployeeWorkSummaryCommand(command.EmployeeId, command.Date.Year, command.Date.Month)).Result;


            items.Add(BuildSummary("Zwolnienia", employeeWorkSummary.Leaves.LeavesTaken));
            items.Add(BuildSummary("Zwolnienia (podczas pracy)", employeeWorkSummary.Leaves.LeavesDuringWork));
            items.Add(BuildSummary("Zwolnienia (odliczenie)", new TimeSpan(hours: 0, minutes: employeeWorkSummary.Leaves.LeaveToReduceFromWorkInMinutes, seconds: 0)));

            items.Add(BuildSummary("Przepracowano", new TimeSpan(0, employeeWorkSummary.MinutesSpent, 0)));
            items.Add(BuildSummary("Urlopy", employeeWorkSummary.HolidaysTaken));
            result.Items = items;

            command.Result = result;
        }

        private EmployeeSummaryItem BuildSummary<TValue>(string title, TValue value)
            where TValue: IConvertible
        {
            return new EmployeeSummaryItem
            {
                Title = title,
                Description = value.ToString(CultureInfo.InvariantCulture)
            };
        }

        private EmployeeSummaryItem BuildSummary(string title, TimeSpan value)
        {
            return new EmployeeSummaryItem
            {
                Title = title,
                Description = string.Format("{0}h {1}m", (int)value.TotalHours, value.Minutes)
            };
        }
    }
}
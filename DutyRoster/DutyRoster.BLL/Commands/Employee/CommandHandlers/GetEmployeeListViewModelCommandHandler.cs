﻿using System.Linq;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Queries.Employees;

namespace DutyRoster.BLL.Commands.Employee.CommandHandlers
{
    internal sealed class GetEmployeeListViewModelCommandHandler : ICommandHandler<GetEmployeeListViewModelCommand>
    {
        private readonly IDatabaseQuery databaseQuery;

        public GetEmployeeListViewModelCommandHandler(IDatabaseQuery databaseQuery)
        {
            this.databaseQuery = databaseQuery;
        }

        public void Handle(GetEmployeeListViewModelCommand command)
        {
            var viewModel = new EmployeeListViewModel();

            var employees = databaseQuery.ExecuteQuery(new GetEmployees())
                .AsEnumerable()
                .Select(s => new EmployeeListItem
                {
                    Id = s.Id,
                    Initials = s.Initials,
                    FullName = s.FirstName + " " + s.LastName
                })
                .ToList();
            viewModel.Items = employees;

            command.Result = viewModel;
        }
    }
}
﻿using DutyRoster.BLL.Infrastructure;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models;
using DutyRoster.DAL.Models.Employees;
using DutyRoster.DAL.Queries.Employees;

namespace DutyRoster.BLL.Commands.Employee.CommandHandlers
{
    internal sealed class UpdateEmployeeViewModelCommandHandler : ICommandHandler<UpdateEmployeeViewModelCommand>
    {
        private readonly IDatabaseQuery databaseQuery;
        private readonly IUnitOfWork unitOfWork;

        public UpdateEmployeeViewModelCommandHandler(IDatabaseQuery databaseQuery, IUnitOfWork unitOfWork)
        {
            this.databaseQuery = databaseQuery;
            this.unitOfWork = unitOfWork;
        }

        public void Handle(UpdateEmployeeViewModelCommand command)
        {
            var employee = databaseQuery.ExecuteQuery(new GetEmployee(command.Id));
            employee.FirstName = command.Model.FirstName;
            employee.LastName = command.Model.LastName;
            employee.Initials = command.Model.Initials;
            employee.WorkPeriod.Start = command.Model.Start.Date;
            employee.WorkPeriod.End = command.Model.End;
            employee.WorkPeriod.EmploymentType = (EmploymentType) command.Model.EmploymentType;

            unitOfWork.Save();
        }
    }
}
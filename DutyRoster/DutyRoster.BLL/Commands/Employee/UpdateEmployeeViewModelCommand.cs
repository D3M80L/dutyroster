﻿using DutyRoster.BLL.Commands.Employee.Models;
using DutyRoster.BLL.Infrastructure;

namespace DutyRoster.BLL.Commands.Employee
{
    public sealed class UpdateEmployeeViewModelCommand : Command
    {
        public UpdateEmployeeViewModelCommand(int id, EmployeeViewModel model)
        {
            Model = model;
            Id = id;
        }

        public int Id { get; }

        public EmployeeViewModel Model { get; }
    }
}

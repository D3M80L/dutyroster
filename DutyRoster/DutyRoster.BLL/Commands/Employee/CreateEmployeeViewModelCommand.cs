﻿using DutyRoster.BLL.Commands.Employee.Models;
using DutyRoster.BLL.Infrastructure;

namespace DutyRoster.BLL.Commands.Employee
{
    public sealed class CreateEmployeeViewModelCommand : Command<EmployeeViewModel>
    {
        public CreateEmployeeViewModelCommand(EmployeeViewModel model)
        {
            Model = model;
        }

        public EmployeeViewModel Model { get; }
    }
}
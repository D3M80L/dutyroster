﻿using DutyRoster.BLL.Commands.Employee.Models;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.DAL.Models;

namespace DutyRoster.BLL.Commands.Employee
{
    public sealed class GetEmployeeViewModelCommand : Command<EmployeeViewModel>
    {
        public GetEmployeeViewModelCommand(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}

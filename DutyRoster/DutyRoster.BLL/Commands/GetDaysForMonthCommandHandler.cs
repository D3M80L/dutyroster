using System;
using System.Collections.Generic;
using System.Linq;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.ViewModels.Scheduler.Models;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Queries;

namespace DutyRoster.BLL.Commands
{
    internal sealed class GetDaysForMonthCommandHandler : ICommandHandler<GetDaysForMonthCommand>
    {
        private readonly IDatabaseQuery databaseQuery;

        public GetDaysForMonthCommandHandler(IDatabaseQuery databaseQuery)
        {
            this.databaseQuery = databaseQuery;
        }

        public void Handle(GetDaysForMonthCommand command)
        {
            var year = command.Year;
            var month = command.Month;
            var firstDayInMonth = new DateTime(year, month, 1);
            var result = new List<MonthDay>();

            var bankHolidays = databaseQuery
                .ExecuteQuery(new GetBankHolidaysForMonthQuery(year, month))
                .ToList();

            var date = firstDayInMonth;
            for (int i = 1; i <= DateTime.DaysInMonth(year, month); ++i)
            {
                var monthDay = new MonthDay();
                monthDay.Day = i;
                monthDay.DayOfWeek = date.DayOfWeek;
                monthDay.IsHoliday = date.DayOfWeek == DayOfWeek.Sunday;

                var bankHoliday = bankHolidays.FirstOrDefault(x => x.Day == i);
                if (bankHoliday != null)
                {
                    monthDay.IsHoliday = true;
                    monthDay.Note = bankHoliday.Note;
                }

                result.Add(monthDay);
                date = date.AddDays(1);
            }

            command.Result = result;
        }
    }
}
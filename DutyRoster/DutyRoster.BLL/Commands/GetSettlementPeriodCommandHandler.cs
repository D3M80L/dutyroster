﻿using System;
using System.Collections.Generic;
using System.Linq;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.Models;
using DutyRoster.BLL.ViewModels.Scheduler.Models;

namespace DutyRoster.BLL.Commands
{
    internal sealed class GetSettlementPeriodCommandHandler : ICommandHandler<GetSettlementPeriodCommand>
    {
        private readonly ICommandBus commandBus;

        public GetSettlementPeriodCommandHandler(ICommandBus commandBus)
        {
            this.commandBus = commandBus;
        }

        public void Handle(GetSettlementPeriodCommand command)
        {
            var result = new SettlementPeriod();
            var months = new List<MonthInfo>();

            if (command.Month % 2 == 1)
            {
                months.Add(GetMonthInfo(command.Year, command.Month));
                months.Add(GetMonthInfo(command.Year, command.Month + 1));
            }
            else
            {
                months.Add(GetMonthInfo(command.Year, command.Month - 1));
                months.Add(GetMonthInfo(command.Year, command.Month));
            }

            result.TotalDays = months.Sum(s => s.TotalDays);
            result.TotalWorkingDays = months.Sum(s => s.WorkingDays);
            result.Months = months;
            result.TotalWorkHours = months
                .SelectMany(m => m.WorkHours)
                .GroupBy(g => g.Title)
                .Select(s =>
                {
                    var res = new MonthWorkHours
                    {
                        Title = s.Key,
                    };

                    var totalMinutes = s.Sum(workHour => workHour.Hours * 60 + workHour.Minutes);
                    res.Hours = totalMinutes / 60;
                    res.Minutes = totalMinutes % 60;

                    return res;
                })
                .ToList();

            command.Result = result;
        }


        private MonthInfo GetMonthInfo(int year, int month)
        {
            var monthInfo = new MonthInfo();
            monthInfo.Month = month;
            monthInfo.Year = year;
            monthInfo.TotalDays = DateTime.DaysInMonth(year, month);
            monthInfo.Title = new DateTime(year, month, 1).ToString("MMMM");

            var daysInMonth = commandBus.Execute(new GetDaysForMonthCommand(year, month)).Result;
            var holidaysAmount = daysInMonth
                .Where(x => x.IsHoliday || x.DayOfWeek == DayOfWeek.Saturday)
                .Select(s =>
                {
                    if (s.IsHoliday && s.DayOfWeek == DayOfWeek.Saturday)
                    {
                        return 2;
                    }
                    return 1;
                })
                .Sum();

            monthInfo.WorkingDays = monthInfo.TotalDays - holidaysAmount;

            var workHours = new List<MonthWorkHours>();
            var totalSevenThirtyFive = monthInfo.WorkingDays * 455;
            var workHoursSevenThirtyFive = new MonthWorkHours
            {
                Hours = totalSevenThirtyFive / 60,
                Minutes = totalSevenThirtyFive % 60,
                Title = "7h35m"
            };
            workHours.Add(workHoursSevenThirtyFive);

            monthInfo.WorkHours = workHours;
            return monthInfo;
        }
    }
}
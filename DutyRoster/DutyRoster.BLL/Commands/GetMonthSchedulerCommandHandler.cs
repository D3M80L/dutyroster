﻿using System;
using System.Collections.Generic;
using System.Linq;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.Models;
using DutyRoster.BLL.ViewModels.Scheduler;
using DutyRoster.BLL.ViewModels.Scheduler.Models;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models;
using DutyRoster.DAL.Queries.Employees;
using DutyRoster.DAL.Queries.Schedulers;
using DutyRoster.DAL.Queries.Schedules;

namespace DutyRoster.BLL.Commands
{
    internal sealed class GetMonthSchedulerCommandHandler : ICommandHandler<GetMonthSchedulerCommand>
    {
        private readonly ICommandBus commandBus;
        private readonly IDatabaseQuery databaseQuery;
        private readonly EmployeeSchedulerDayFactory factory = new EmployeeSchedulerDayFactory();

        public GetMonthSchedulerCommandHandler(ICommandBus commandBus, IDatabaseQuery databaseQuery)
        {
            this.commandBus = commandBus;
            this.databaseQuery = databaseQuery;
        }

        public void Handle(GetMonthSchedulerCommand command)
        {
            var monthScheduler = BuildMonthScheduler(command.Date);
            var availableEmployees = GetEmployees(command.Date);

            var employeeMonthInfos = new List<EmployeeMonthInfo>();
            foreach (var employee in availableEmployees)
            {
                var employeeMonthInfo = GetEmployeeWorkPlan(command.Date, employee);
                employeeMonthInfos.Add(employeeMonthInfo);
            }

            monthScheduler.Employees = employeeMonthInfos;

            monthScheduler.Days = commandBus.Execute(new GetDaysForMonthCommand(command.Date.Year, command.Date.Month)).Result;

            command.Result = monthScheduler;
        }

        private EmployeeMonthInfo GetEmployeeWorkPlan(DateTime date, DAL.Models.Employees.Employee employee)
        {
            var employeeWork = databaseQuery
                .ExecuteQuery(new GetEmployeeWorkPeriodsInMonth(employee.Id, date))
                .SingleOrDefault();

            var employeeMonthSummary = databaseQuery
                .ExecuteQuery(new GetEmployeeMonthSummary(employee.Id, date.Year, date.Month));

            var employeeMonthInfo = new EmployeeMonthInfo
            {
                EmployeeId = employee.Id,
                Initials = employee.Initials,
                MinutesToWork = employeeMonthSummary.WorkForNextMonth
            };

            var dayList = new List<EmployeeDayInfo>();

            var employeeDays = databaseQuery
                .ExecuteQuery(new GetEmployeeDayForMonth(employee.Id, date))
                .ToList();

            var daysInMonth = DateTime.DaysInMonth(date.Year, date.Month);
            for (int day = 1; day <= daysInMonth; ++day)
            {
                var employeeDayInfo = factory.Get(day, employeeDays, employeeWork.EmploymentType);

                dayList.Add(employeeDayInfo);
            }

            employeeMonthInfo.Days = dayList;
            return employeeMonthInfo;
        }

        private List<DAL.Models.Employees.Employee> GetEmployees(DateTime date)
        {
            var availableEmployees = databaseQuery
                .ExecuteQuery(new GetAvailableEmployeesInMonth(date.Year, date.Month))
                .ToList();
            return availableEmployees;
        }

        private MonthScheduler BuildMonthScheduler(DateTime date)
        {
            var monthScheduler = new MonthScheduler();
            monthScheduler.TotalDays = DateTime.DaysInMonth(date.Year, date.Month);
            monthScheduler.Title = date.ToString("MMMM");
            monthScheduler.Year = date.Year;
            monthScheduler.Month = date.Month;

            var monthSchedulerInfo = databaseQuery.ExecuteQuery(new GetMonthSchedulerInfo(date.Year, date.Month));

            monthScheduler.IsLocked = monthSchedulerInfo.IsLocked;
            return monthScheduler;
        }
    }
}
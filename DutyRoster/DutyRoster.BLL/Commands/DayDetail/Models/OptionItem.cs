namespace DutyRoster.BLL.Commands.DayDetail.Models
{
    public class OptionItem
    {
        public int Id { get; set; }

        public string Title { get; set; }
    }
}
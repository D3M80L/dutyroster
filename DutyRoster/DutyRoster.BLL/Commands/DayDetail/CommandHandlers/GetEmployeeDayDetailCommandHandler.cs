using System;
using System.Collections.Generic;
using DutyRoster.BLL.Commands.DayDetail.Models;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.Models.DayDetail;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Queries.Schedulers;

namespace DutyRoster.BLL.Commands.DayDetail.CommandHandlers
{
    internal sealed class GetEmployeeDayDetailCommandHandler : ICommandHandler<GetEmployeeDayDetailCommand>
    {
        private readonly IDatabaseQuery databaseQuery;

        public GetEmployeeDayDetailCommandHandler(IDatabaseQuery databaseQuery)
        {
            this.databaseQuery = databaseQuery;
        }

        public void Handle(GetEmployeeDayDetailCommand command)
        {
            var employee = databaseQuery
                .ExecuteQuery(new GetEmployeeAvailableInMonth(command.EmployeeId, command.Date));

            var employeeDayEntity = databaseQuery
                .ExecuteQuery(new GetEmployeeDay(command.EmployeeId, command.Date));

            var result = new EmployeeDayDetail();
            result.Date = command.Date;
            result.MonthName = command.Date.ToString("MMMM");
            result.EmployeeId = command.EmployeeId;
            result.FullName = employee.LastName + " " + employee.LastName;

            if (employeeDayEntity != null)
            {
                result.Description = employeeDayEntity.Note;
                result.DayType = (EmployeeDayType)employeeDayEntity.DayInfo;
                result.Start = employeeDayEntity.Start;
                result.End = employeeDayEntity.End;
                result.IsFixed = employeeDayEntity.IsReadOnly;
                result.IsLeave = employeeDayEntity.IsLeave;
            }
            else
            {
                result.Start = DateTime.Now.Date;
                result.End = DateTime.Now;
            }

            SetDayTypes(result);
            command.Result = result;
        }

        private static void SetDayTypes(EmployeeDayDetail result)
        {
            var dayTypes = new List<OptionItem>();
            dayTypes.Add(new OptionItem { Id = (int)EmployeeDayType.Unknown, Title = "Nieznany" });
            dayTypes.Add(new OptionItem { Id = (int)EmployeeDayType.Work, Title = "Pracuj�cy" });
            dayTypes.Add(new OptionItem { Id = (int)EmployeeDayType.Holiday, Title = "Urlop" });
            result.DayTypes = dayTypes;
        }
    }
}
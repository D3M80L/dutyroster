﻿using System;
using DutyRoster.BLL.Commands.Schedule;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.Services;
using DutyRoster.DAL.Commands;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models;
using DutyRoster.DAL.Models.EmployeeDays;
using DutyRoster.DAL.Queries.Schedulers;

namespace DutyRoster.BLL.Commands.DayDetail.CommandHandlers
{
    internal sealed class SetEmployeeDayDetailCommandHandler : ICommandHandler<SetEmployeeDayDetailCommand>
    {
        private readonly IDatabaseQuery databaseQuery;
        private readonly IDatabaseCommand databaseCommand;
        private readonly IUnitOfWork unitOfWork;
        private readonly ICommandBus commandBus;

        public SetEmployeeDayDetailCommandHandler(
            IDatabaseQuery databaseQuery, 
            IDatabaseCommand databaseCommand, 
            IUnitOfWork unitOfWork, 
            ICommandBus commandBus)
        {
            this.databaseQuery = databaseQuery;
            this.databaseCommand = databaseCommand;
            this.unitOfWork = unitOfWork;
            this.commandBus = commandBus;
        }

        public void Handle(SetEmployeeDayDetailCommand command)
        {
            var model = command.Model;

            var employeeDayEntity = databaseQuery
                .ExecuteQuery(new GetEmployeeDay(model.EmployeeId, model.Date));

            if (employeeDayEntity == null)
            {
                employeeDayEntity = databaseCommand
                    .ExecuteCommand(new AddEmployeeDayDataCommand(model.EmployeeId, model.Date));
            }

            if (employeeDayEntity.IsReadOnly == false)
            {
                var startDate = new DateTime(model.Date.Year, model.Date.Month, model.Date.Day, model.Start.Hour, model.Start.Minute, 0);
                var endDate = new DateTime(model.Date.Year, model.Date.Month, model.Date.Day, model.End.Hour, model.End.Minute, 0);

                employeeDayEntity.Start = startDate;
                employeeDayEntity.End = EmployeeDateService.GetEndDate(employeeDayEntity.Start, endDate);
                employeeDayEntity.DayInfo = (DayInfo)model.DayType;
                employeeDayEntity.IsLeave = model.IsLeave;
            }

            employeeDayEntity.IsReadOnly = model.IsFixed;
            employeeDayEntity.Note = model.Description;

            unitOfWork.Save();

            commandBus.Execute(new RecalculateEmployeeWorkForMonthCommand(model.EmployeeId, model.Date.Year, model.Date.Month));
        }
    }
}

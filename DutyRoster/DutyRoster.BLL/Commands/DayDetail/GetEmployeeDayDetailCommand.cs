﻿using System;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.Models.DayDetail;

namespace DutyRoster.BLL.Commands.DayDetail
{
    public sealed class GetEmployeeDayDetailCommand : Command<EmployeeDayDetail>
    {
        public GetEmployeeDayDetailCommand(int employeeId, int year, int month, int day) : this(employeeId, new DateTime(year, month, day))
        {
            
        }

        public GetEmployeeDayDetailCommand(int employeeId, DateTime date)
        {
            EmployeeId = employeeId;
            Date = date;
        }

        public int EmployeeId { get; }

        public DateTime Date { get; }
    }
}

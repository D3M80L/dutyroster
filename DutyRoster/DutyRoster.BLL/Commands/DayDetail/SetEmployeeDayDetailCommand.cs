﻿using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.Models.DayDetail;

namespace DutyRoster.BLL.Commands.DayDetail
{
    public sealed class SetEmployeeDayDetailCommand : Command
    {
        public SetEmployeeDayDetailCommand(EmployeeDayDetail model)
        {
            Model = model;
        }

        public EmployeeDayDetail Model { get; }
    }
}
﻿using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.Models;
using DutyRoster.BLL.ViewModels.Scheduler.Models;

namespace DutyRoster.BLL.Commands
{
    public sealed class SetEmployeeSchedulerDayInfoCommand : Command
    {
        public SetEmployeeSchedulerDayInfoCommand(EmployeeSchedulerDayInfo dayInfo)
        {
            DayInfo = dayInfo;
        }

        public EmployeeSchedulerDayInfo DayInfo { get; }
    }
}

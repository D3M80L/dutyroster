﻿using System;
using System.Linq;
using DutyRoster.BLL.Commands.Schedule;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.Models;
using DutyRoster.BLL.ViewModels.Scheduler.Models;
using DutyRoster.DAL.Commands;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models;
using DutyRoster.DAL.Models.EmployeeDays;
using DutyRoster.DAL.Models.Employees;
using DutyRoster.DAL.Queries;
using DutyRoster.DAL.Queries.Employees;
using DutyRoster.DAL.Queries.Schedulers;

namespace DutyRoster.BLL.Commands
{
    internal sealed class SetEmployeeSchedulerDayInfoCommandHandler : ICommandHandler<SetEmployeeSchedulerDayInfoCommand>
    {
        private readonly IDatabaseQuery databaseQuery;
        private readonly IDatabaseCommand databaseCommand;
        private readonly IUnitOfWork unitOfWork;
        private readonly ICommandBus commandBus;

        public SetEmployeeSchedulerDayInfoCommandHandler(IDatabaseQuery databaseQuery, IDatabaseCommand databaseCommand, IUnitOfWork unitOfWork, ICommandBus commandBus)
        {
            this.databaseQuery = databaseQuery;
            this.databaseCommand = databaseCommand;
            this.unitOfWork = unitOfWork;
            this.commandBus = commandBus;
        }

        public void Handle(SetEmployeeSchedulerDayInfoCommand command)
        {
            var dayInfo = command.DayInfo;
            // TODO: Check if user works this day
            var date = new DateTime(dayInfo.Year, dayInfo.Month, dayInfo.Day);
            var startDate = new DateTime(dayInfo.Year, dayInfo.Month, 1);

            var employeeWorkPeriod = databaseQuery
                .ExecuteQuery(new GetEmployeeWorkPeriodsInMonth(dayInfo.EmployeeId, startDate))
                .SingleOrDefault();

            if (employeeWorkPeriod == null)
            {
                throw new InvalidOperationException("Employee not found");
            }

            var employeeWork = databaseQuery
                .ExecuteQuery(new GetEmployeeDay(dayInfo.EmployeeId, date));

            if (employeeWork != null && employeeWork.IsReadOnly)
            {
                return;
            }

            if (employeeWork == null)
            {
                employeeWork = databaseCommand
                    .ExecuteCommand(new AddEmployeeDayDataCommand(dayInfo.EmployeeId, date));
            }

            if (dayInfo.DayType == DayType.Undefined)
            {
                employeeWork.DayInfo = DayInfo.Undefined;
            }
            else if (dayInfo.DayType == DayType.Holiday)
            {
                employeeWork.DayInfo = DayInfo.Holiday;
                employeeWork.Start = date;
                employeeWork.End = date.AddDays(1).AddSeconds(-1);
            }
            else if (dayInfo.DayType == DayType.Custom)
            {
                employeeWork.DayInfo = DayInfo.Work;
                employeeWork.Start = new DateTime(date.Year, date.Month, date.Day, dayInfo.Start.Hours, dayInfo.Start.Minutes, 0);

                var endDate = date;
                if (dayInfo.End <= dayInfo.Start)
                {
                    endDate = date.AddDays(1);
                }
                employeeWork.End = new DateTime(endDate.Year, endDate.Month, endDate.Day, dayInfo.End.Hours, dayInfo.End.Minutes, 0);
            }
            else
            {
                employeeWork.DayInfo = DayInfo.Work;
                if (dayInfo.DayType == DayType.DayShift)
                {
                    employeeWork.Start = date.AddHours(7);
                    if (employeeWorkPeriod.EmploymentType == EmploymentType.FixedContractOfEmployment)
                    {
                        employeeWork.End = date.AddHours(14).AddMinutes(35);
                    }
                    else
                    {
                        employeeWork.End = date.AddHours(19);
                    }
                }
                else if (dayInfo.DayType == DayType.NightShift)
                {
                    employeeWork.Start = date.AddHours(19);
                    employeeWork.End = date.AddDays(1).AddHours(7);
                }
                else if (dayInfo.DayType == DayType.FullDay)
                {
                    employeeWork.Start = date.AddHours(7);
                    employeeWork.End = date.AddDays(1).AddHours(7);
                }
            }

            unitOfWork.Save();

            commandBus.Execute(new RecalculateEmployeeWorkForMonthCommand(command.DayInfo.EmployeeId, command.DayInfo.Year, command.DayInfo.Month));
        }
    }
}
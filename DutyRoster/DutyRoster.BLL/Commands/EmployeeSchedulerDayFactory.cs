﻿using System.Collections.Generic;
using System.Linq;
using DutyRoster.BLL.Models;
using DutyRoster.BLL.ViewModels.Scheduler.Models;
using DutyRoster.DAL.Models;
using DutyRoster.DAL.Models.EmployeeDays;
using DutyRoster.DAL.Models.Employees;

namespace DutyRoster.BLL.Commands
{
    public sealed class EmployeeSchedulerDayFactory
    {
        internal EmployeeDayInfo Get(int day, IEnumerable<EmployeeDay> employeeDays, EmploymentType employmentType)
        {
            var employeeDayInfo = new EmployeeDayInfo();
            employeeDayInfo.Day = day;

            var employeeDay = employeeDays
                .FirstOrDefault(x => x.Start.Day == day);

            if (employeeDay != null)
            {
                employeeDayInfo.IsFixed = employeeDay.IsReadOnly;
                employeeDayInfo.Start = employeeDay.Start.TimeOfDay;
                employeeDayInfo.End = employeeDay.End.TimeOfDay;
                employeeDayInfo.HasLeave = employeeDay.IsLeave;
                employeeDayInfo.HasNote = !string.IsNullOrWhiteSpace(employeeDay.Note);
                if (employeeDay.DayInfo == DayInfo.Undefined)
                {
                    employeeDayInfo.DayType = DayType.Undefined;
                }
                else if (employeeDay.DayInfo == DayInfo.Holiday)
                {
                    employeeDayInfo.DayType = DayType.Holiday;
                }
                else
                {
                    if (employmentType == EmploymentType.FixedContractOfEmployment)
                    {
                        employeeDayInfo.DayType = GetDayTypeForFixedContract(employeeDay);
                    }
                    else
                    {
                        employeeDayInfo.DayType = GetDayType(employeeDay);
                    }
                }
            }
            return employeeDayInfo;
        }

        private DayType GetDayTypeForFixedContract(EmployeeDay employeeDay)
        {
            if (employeeDay.Start.Hour == 7 && employeeDay.Start.Minute == 0)
            {
                if (employeeDay.End.Hour == 14 && employeeDay.End.Minute == 35)
                {
                    return DayType.DayShift;
                }
            }

            return DayType.Custom;
        }

        private DayType GetDayType(EmployeeDay employeeDay)
        {
            if (employeeDay.Start.Hour == 7 && employeeDay.Start.Minute == 0)
            {
                if (employeeDay.End.Hour == 19 && employeeDay.End.Minute == 0)
                {
                    return DayType.DayShift;
                }

                if (employeeDay.End.Hour == 7 && employeeDay.End.Minute == 0)
                {
                    return DayType.FullDay;
                }
            }

            if (employeeDay.Start.Hour == 19 && employeeDay.Start.Minute == 0)
            {
                if (employeeDay.End.Hour == 7 && employeeDay.End.Minute == 0)
                {
                    return DayType.NightShift;
                }
            }

            return DayType.Custom;
        }
    }
}

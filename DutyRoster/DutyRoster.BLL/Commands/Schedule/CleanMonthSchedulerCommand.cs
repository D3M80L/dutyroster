using System;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.DAL.Queries;

namespace DutyRoster.BLL.Commands.Schedule
{
    public sealed class CleanMonthSchedulerCommand : Command
    {
        public CleanMonthSchedulerCommand(int year, int month)
        {
            Date = new DateTime(year, month, 1);
        }

        public DateTime Date { get; }
    }
}
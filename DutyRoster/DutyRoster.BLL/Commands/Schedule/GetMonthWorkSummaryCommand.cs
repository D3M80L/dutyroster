﻿using System;
using System.Collections.Generic;
using System.Linq;
using DutyRoster.BLL.DataContracts;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Queries;
using DutyRoster.DAL.Queries.Schedulers;
using DutyRoster.DAL.Queries.Schedules;

namespace DutyRoster.BLL.Commands.Schedule
{
    public sealed class GetMonthWorkSummaryCommand : Command<MonthWorkSummaryViewModel>
    {
        public GetMonthWorkSummaryCommand(int year, int month)
        {
            Year = year;
            Month = month;
        }

        public int Year { get; }

        public int Month { get; }
    }

    public sealed class MonthWorkSummaryViewModel
    {
        public bool IsLocked { get; set; }

        public DateTime Date { get; set; }

        public IEnumerable<EmployeeMonthSummaryItem> Items { get; set; }
    }

    internal sealed class GetMonthWorkSummaryCommandHandler : ICommandHandler<GetMonthWorkSummaryCommand>
    {
        private readonly IDatabaseQuery databaseQuery;
        private readonly ICommandBus commandBus;

        public GetMonthWorkSummaryCommandHandler(IDatabaseQuery databaseQuery, ICommandBus commandBus)
        {
            this.databaseQuery = databaseQuery;
            this.commandBus = commandBus;
        }

        public void Handle(GetMonthWorkSummaryCommand command)
        {
            var viewModel = new MonthWorkSummaryViewModel();
            var monthSchedulerInfo = databaseQuery.ExecuteQuery(new GetMonthSchedulerInfo(command.Year, command.Month));
            viewModel.IsLocked = monthSchedulerInfo.IsLocked;
            viewModel.Date = new DateTime(command.Year, command.Month, 1);
            var employeeMonthSummaryItems = new List<EmployeeMonthSummaryItem>();

            var availableEmployees = databaseQuery.ExecuteQuery(new GetAvailableEmployeesInMonth(command.Year, command.Month))
                .ToList();

            foreach (var employee in availableEmployees)
            {
                var item = new EmployeeMonthSummaryItem();
                item.EmployeeId = employee.Id;
                item.FullName = employee.FirstName + " " + employee.LastName;

                var employeeWork = commandBus.Execute(new GetEmployeeWorkForMonthCommand(employee.Id, command.Year, command.Month)).Result;

                item.MinutesToWork = employeeWork.MinutesToWork;
                item.WorkFromPreviousMonth = employeeWork.WorkFromPreviousMonth;
                item.HolidaysTaken = employeeWork.HolidaysTaken;
                item.LeavesTaken = employeeWork.LeavesTaken;
                item.LeavesLenght = employeeWork.LeavesLenght;
                item.WorkPlanned = employeeWork.WorkPlanned;
                item.WorkForNextMonth = employeeWork.WorkForNextMonth;

                employeeMonthSummaryItems.Add(item);
            }

            viewModel.Items = employeeMonthSummaryItems;
            command.Result = viewModel;
        }
    }

    public sealed class EmployeeMonthSummaryItem : IWorkFromPreviousMonth, IEmployeeId
    {
        public int EmployeeId { get; set; }
        public string FullName { get; set; }

        public int MinutesToWork { get; set; }

        public int HolidaysTaken { get; set; }

        public int LeavesTaken { get; set; }

        public int WorkFromPreviousMonth { get; set; }

        public int WorkForNextMonth { get; set; }

        public int LeavesLenght { get; set; }

        public int WorkPlanned { get; set; }
    }
}

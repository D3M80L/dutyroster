﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DutyRoster.BLL.DataContracts;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Queries;
using DutyRoster.DAL.Queries.Employees;
using DutyRoster.DAL.Queries.Schedulers;
using DutyRoster.DAL.Queries.Schedules;

namespace DutyRoster.BLL.Commands.Schedule
{
    public sealed class SetEmployeesMonthWorkSummaryCommand : Command
    {
        public SetEmployeesMonthWorkSummaryCommand(int year, int month, EmployeesMonthWorkSummary model)
        {
            Model = model;
            Year = year;
            Month = month;
        }

        public int Year { get; }

        public int Month { get; }

        public EmployeesMonthWorkSummary Model { get; }
    }

    public sealed class EmployeesMonthWorkSummary
    {
        public IEnumerable<EmployeeMonthSummaryData> Items { get; set; }
    }

    public sealed class EmployeeMonthSummaryData : IWorkFromPreviousMonth, IEmployeeId
    {
        public int WorkFromPreviousMonth { get; set; }

        public int EmployeeId { get; set; }
    }

    internal sealed class SetEmployeesMonthWorkSummaryCommandHandler : ICommandHandler<SetEmployeesMonthWorkSummaryCommand>
    {
        private readonly IDatabaseQuery databaseQuery;
        private readonly IUnitOfWork unitOfWork;
        private readonly ICommandBus commandBus;

        public SetEmployeesMonthWorkSummaryCommandHandler(IDatabaseQuery databaseQuery, IUnitOfWork unitOfWork, ICommandBus commandBus)
        {
            this.databaseQuery = databaseQuery;
            this.unitOfWork = unitOfWork;
            this.commandBus = commandBus;
        }

        public void Handle(SetEmployeesMonthWorkSummaryCommand command)
        {
            foreach (var item in command.Model.Items)
            {
                var employeeMonthSummary = databaseQuery.ExecuteQuery(new GetEmployeeMonthSummary(item.EmployeeId, command.Year, command.Month));
                if (employeeMonthSummary.IsLocked == false)
                {
                    employeeMonthSummary.WorkFromPreviousMonth = item.WorkFromPreviousMonth;

                    unitOfWork.Save();
                    commandBus.Execute(new RecalculateEmployeeWorkForMonthCommand(item.EmployeeId, command.Year, command.Month));
                }
            }
        }
    }
}

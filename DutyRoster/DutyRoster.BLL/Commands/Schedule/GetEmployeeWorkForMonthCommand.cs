﻿using System.Linq;
using DutyRoster.BLL.Commands.Month;
using DutyRoster.BLL.Commands.Schedule.Models;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models;
using DutyRoster.DAL.Models.EmployeeDays;
using DutyRoster.DAL.Queries;
using DutyRoster.DAL.Queries.Employees;
using DutyRoster.DAL.Queries.Schedulers;
using DutyRoster.DAL.Queries.Schedules;

namespace DutyRoster.BLL.Commands.Schedule
{
    public sealed class GetEmployeeWorkForMonthCommand : Command<EmployeeWork>
    {
        public GetEmployeeWorkForMonthCommand(int employeeId, int year, int month)
        {
            EmployeeId = employeeId;
            Year = year;
            Month = month;
        }

        public int EmployeeId { get; }

        public int Year { get; }

        public int Month { get; }
    }

    internal sealed class GetEmployeeWorkForMonthCommandHandler : ICommandHandler<GetEmployeeWorkForMonthCommand>
    {
        private const int SevenThirtyFive = 7 * 60 + 35;
        private readonly IDatabaseQuery databaseQuery;
        private readonly ICommandBus commandBus;

        public GetEmployeeWorkForMonthCommandHandler(IDatabaseQuery databaseQuery, ICommandBus commandBus)
        {
            this.databaseQuery = databaseQuery;
            this.commandBus = commandBus;
        }

        public void Handle(GetEmployeeWorkForMonthCommand command)
        {
            var employeeWork = new EmployeeWork();
            command.Result = employeeWork;

            var employeeMonthSummary = databaseQuery.ExecuteQuery(new GetEmployeeMonthSummary(command.EmployeeId, command.Year, command.Month));
            employeeWork.WorkFromPreviousMonth = employeeMonthSummary.WorkFromPreviousMonth;
            employeeWork.WorkForNextMonth = employeeMonthSummary.WorkForNextMonth;
            employeeWork.MinutesToWork = employeeMonthSummary.MinimumWork;
            employeeWork.HolidaysTaken = employeeMonthSummary.HolidaysTaken;
            employeeWork.LeavesTaken = employeeMonthSummary.LeavesTaken;
            employeeWork.LeavesLenght = employeeMonthSummary.LeavesLenght;
            employeeWork.WorkPlanned = employeeMonthSummary.WorkInMinutes;

            if (employeeMonthSummary.IsLocked)
            {
                return;
            }

            SetMinutesToWork(command, employeeWork);

            var days = databaseQuery.ExecuteQuery(new GetEmployeeDayForMonth(command.EmployeeId, command.Year, command.Month)).ToList();

            employeeWork.HolidaysTaken = days.Count(x => x.DayInfo == DayInfo.Holiday);
            employeeWork.WorkPlanned = days
                    .Where(x => x.DayInfo == DayInfo.Work)
                    .Select(s => (int)(s.End - s.Start).TotalMinutes)
                    .Sum();

            employeeWork.WorkForNextMonth = employeeWork.MinutesToWork + employeeWork.WorkFromPreviousMonth
                                    - employeeWork.WorkPlanned
                                    - employeeWork.HolidaysTaken * SevenThirtyFive;

            employeeWork.LeavesTaken = days.Count(x => x.DayInfo == DayInfo.Work && x.IsLeave);
            employeeWork.LeavesLenght = days
                .Where(x => x.DayInfo == DayInfo.Work && x.IsLeave)
                .Select(s => (int) (s.End - s.Start).TotalMinutes)
                .Sum();

            command.Result = employeeWork;
        }

        private void SetMinutesToWork(GetEmployeeWorkForMonthCommand command, EmployeeWork employeeWork)
        {
            if (employeeWork.MinutesToWork != 0)
            {
                return;
            }

            var employee = databaseQuery.ExecuteQuery(new GetEmployee(command.EmployeeId));

            var workingDaysAmount = commandBus.Execute(new GetWorkingDaysAmountCommand(command.Year, command.Month)).Result;
            employeeWork.MinutesToWork = employee.WorkPeriod.MinumumWorkPerMonthInMinutes.GetValueOrDefault(SevenThirtyFive * workingDaysAmount.WorkingDays);
        }
    }
}
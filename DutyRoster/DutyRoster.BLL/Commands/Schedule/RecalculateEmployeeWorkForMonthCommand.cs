using System;
using DutyRoster.BLL.Commands.Schedule.Models;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Queries.Schedules;

namespace DutyRoster.BLL.Commands.Schedule
{
    public sealed class RecalculateEmployeeWorkForMonthCommand : Command<EmployeeWork>
    {
        public RecalculateEmployeeWorkForMonthCommand(int employeeId, int year, int month)
        {
            EmployeeId = employeeId;
            Year = year;
            Month = month;
        }

        public int EmployeeId { get; }

        public int Year { get; }

        public int Month { get; }
    }

    internal sealed class RecalculateEmployeeWorkForMonthCommandHandler : ICommandHandler<RecalculateEmployeeWorkForMonthCommand>
    {
        private readonly IDatabaseQuery databaseQuery;
        private readonly ICommandBus commandBus;
        private readonly IUnitOfWork unitOfWork;

        public RecalculateEmployeeWorkForMonthCommandHandler(ICommandBus commandBus, IDatabaseQuery databaseQuery, IUnitOfWork unitOfWork)
        {
            this.commandBus = commandBus;
            this.databaseQuery = databaseQuery;
            this.unitOfWork = unitOfWork;
        }

        public void Handle(RecalculateEmployeeWorkForMonthCommand command)
        {
            var employeeMontSummaryEntity = databaseQuery.ExecuteQuery(new GetEmployeeMonthSummary(command.EmployeeId, command.Year, command.Month));
            if (employeeMontSummaryEntity.IsLocked)
            {
                throw new InvalidOperationException("Cannot recalculate for an Locked entity.");
            }

            var employeeWorkForMonth = commandBus.Execute(new GetEmployeeWorkForMonthCommand(command.EmployeeId, command.Year, command.Month)).Result;
            employeeMontSummaryEntity.MinimumWork = employeeWorkForMonth.MinutesToWork;
            employeeMontSummaryEntity.WorkForNextMonth = employeeWorkForMonth.WorkForNextMonth;
            employeeMontSummaryEntity.LeavesTaken = employeeWorkForMonth.LeavesTaken;
            employeeMontSummaryEntity.LeavesLenght = employeeWorkForMonth.LeavesLenght;
            employeeMontSummaryEntity.HolidaysTaken = employeeWorkForMonth.HolidaysTaken;
            unitOfWork.Save();

            command.Result = employeeWorkForMonth;
        }
    }
}
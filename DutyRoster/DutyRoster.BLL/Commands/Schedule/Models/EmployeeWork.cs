using DutyRoster.BLL.DataContracts;

namespace DutyRoster.BLL.Commands.Schedule.Models
{
    public sealed class EmployeeWork : IWorkFromPreviousMonth
    {
        public int MinutesToWork { get; set; }
        public int WorkPlanned { get; set; }
        public int WorkFromPreviousMonth { get; set; }
        public int WorkForNextMonth { get; set; }
        public int HolidaysTaken { get; set; }
        public int LeavesTaken { get; set; }
        public int LeavesLenght { get; set; }
    }
}
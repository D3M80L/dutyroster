﻿using DutyRoster.BLL.Infrastructure;

namespace DutyRoster.BLL.Commands.Schedule
{
    public sealed class LockSchedulerMonthCommand : Command
    {
        public LockSchedulerMonthCommand(int year, int month)
        {
            Year = year;
            Month = month;
        }

        public int Year { get; }

        public int Month { get; }
    }
}

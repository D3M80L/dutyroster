﻿using System;
using DutyRoster.BLL.Infrastructure;

namespace DutyRoster.BLL.Commands.Schedule
{
    public sealed class AutoplanMonthChedulerCommand : Command
    {
        public AutoplanMonthChedulerCommand(int year, int month)
        {
            Date = new DateTime(year, month, 1);
        }

        public DateTime Date { get; }
    }
}
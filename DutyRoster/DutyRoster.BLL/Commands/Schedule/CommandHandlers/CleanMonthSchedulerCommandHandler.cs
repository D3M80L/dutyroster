using System.Linq;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models;
using DutyRoster.DAL.Models.EmployeeDays;
using DutyRoster.DAL.Queries.Employees;
using DutyRoster.DAL.Queries.Schedulers;

namespace DutyRoster.BLL.Commands.Schedule.CommandHandlers
{
    public sealed class CleanMonthSchedulerCommandHandler : ICommandHandler<CleanMonthSchedulerCommand>
    {
        private readonly IDatabaseQuery databaseQuery;
        private readonly IUnitOfWork unitOfWork;
        private readonly ICommandBus commandBus;

        public CleanMonthSchedulerCommandHandler(IDatabaseQuery databaseQuery, IUnitOfWork unitOfWork, ICommandBus commandBus)
        {
            this.databaseQuery = databaseQuery;
            this.unitOfWork = unitOfWork;
            this.commandBus = commandBus;
        }

        public void Handle(CleanMonthSchedulerCommand command)
        {
            var availableEmployees = databaseQuery
                .ExecuteQuery(new GetAvailableEmployeesInMonth(command.Date.Year, command.Date.Month))
                .ToList();

            foreach (var employee in availableEmployees)
            {
                var employeeWork = databaseQuery
                    .ExecuteQuery(new GetEmployeeWorkPeriodsInMonth(employee.Id, command.Date))
                    .SingleOrDefault();

                var employeeDays = databaseQuery
                    .ExecuteQuery(new GetEmployeeDayForMonth(employee.Id, command.Date))
                    .ToList();

                foreach (var employeeDay in employeeDays)
                {
                    if (employeeDay.IsReadOnly == false)
                    {
                        employeeDay.DayInfo = DayInfo.Undefined;
                    }
                }

                unitOfWork.Save();

                commandBus.Execute(new RecalculateEmployeeWorkForMonthCommand(employee.Id, command.Date.Year, command.Date.Month));
            }
        }
    }
}
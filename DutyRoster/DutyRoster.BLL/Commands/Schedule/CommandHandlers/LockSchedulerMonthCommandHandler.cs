﻿using System;
using System.Linq;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Queries.Schedulers;
using DutyRoster.DAL.Queries.Schedules;

namespace DutyRoster.BLL.Commands.Schedule.CommandHandlers
{
    internal sealed class LockSchedulerMonthCommandHandler : ICommandHandler<LockSchedulerMonthCommand>
    {
        private readonly IDatabaseQuery databaseQuery;
        private readonly IUnitOfWork unitOfWork;
        private readonly ICommandBus commandBus;

        public LockSchedulerMonthCommandHandler(IDatabaseQuery databaseQuery, ICommandBus commandBus, IUnitOfWork unitOfWork)
        {
            this.databaseQuery = databaseQuery;
            this.commandBus = commandBus;
            this.unitOfWork = unitOfWork;
        }

        public void Handle(LockSchedulerMonthCommand command)
        {
            var monthInfo = databaseQuery
                .ExecuteQuery(new GetMonthSchedulerInfo(command.Year, command.Month));
            if (monthInfo.IsLocked)
            {
                throw new InvalidOperationException("Cannot modify and Locked entity.");
            }

            var availableEmployees = databaseQuery
                .ExecuteQuery(new GetAvailableEmployeesInMonth(command.Year, command.Month))
                .ToList();

            var nextMonth = new DateTime(command.Year, command.Month, 1).AddMonths(1);
            foreach (var employee in availableEmployees)
            {
                var employeeWork = commandBus
                    .Execute(new RecalculateEmployeeWorkForMonthCommand(employee.Id, command.Year, command.Month)).Result;

                var nextMonthEmployeeWorkSummary = databaseQuery
                    .ExecuteQuery(new GetEmployeeMonthSummary(employee.Id, nextMonth.Year, nextMonth.Month));
                nextMonthEmployeeWorkSummary.WorkFromPreviousMonth = employeeWork.WorkForNextMonth;

                var currentMonthEmployeeWorkSummary = databaseQuery.ExecuteQuery(new GetEmployeeMonthSummary(employee.Id, command.Year, command.Month));
                currentMonthEmployeeWorkSummary.IsLocked = true;
                unitOfWork.Save();
            }

            monthInfo.IsLocked = true;
            unitOfWork.Save();
        }
    }
}
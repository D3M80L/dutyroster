﻿using System;
using System.Collections.Generic;
using System.Linq;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.ViewModels.Scheduler.Models;
using DutyRoster.DAL.Commands;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models.EmployeeDays;
using DutyRoster.DAL.Models.Employees;
using DutyRoster.DAL.Queries.Employees;
using DutyRoster.DAL.Queries.Schedulers;

namespace DutyRoster.BLL.Commands.Schedule.CommandHandlers
{
    internal sealed class AutoplanMonthChedulerCommandHandler : ICommandHandler<AutoplanMonthChedulerCommand>
    {
        private readonly IDatabaseQuery databaseQuery;
        private readonly IDatabaseCommand databaseCommand;
        private readonly IUnitOfWork unitOfWork;
        private readonly ICommandBus commandBus;

        public AutoplanMonthChedulerCommandHandler(IDatabaseQuery databaseQuery, IUnitOfWork unitOfWork, ICommandBus commandBus, IDatabaseCommand databaseCommand)
        {
            this.databaseQuery = databaseQuery;
            this.unitOfWork = unitOfWork;
            this.commandBus = commandBus;
            this.databaseCommand = databaseCommand;
        }

        public void Handle(AutoplanMonthChedulerCommand command)
        {
            var availableEmployees = databaseQuery
                .ExecuteQuery(new GetAvailableEmployeesInMonth(command.Date.Year, command.Date.Month))
                .ToList();

            var daysInMonth = commandBus.Execute(new GetDaysForMonthCommand(command.Date.Year, command.Date.Month)).Result;

            foreach (var employee in availableEmployees)
            {
                var employeeWork = databaseQuery
                    .ExecuteQuery(new GetEmployeeWorkPeriodsInMonth(employee.Id, command.Date))
                    .Single();

                if (employeeWork.EmploymentType == EmploymentType.FixedContractOfEmployment)
                {
                    PlanFixedContractOfEmployment(command, employee, daysInMonth);
                }
                else if (employeeWork.EmploymentType == EmploymentType.ContractOfEmployment)
                {
                    PlanContractOfEmployment(command, employee, daysInMonth);
                }
                else if (employeeWork.EmploymentType == EmploymentType.BusinessToBusiness)
                {
                    PlanBusinessToBusiness(command, employee, daysInMonth);
                }

                unitOfWork.Save();
                commandBus.Execute(new RecalculateEmployeeWorkForMonthCommand(employee.Id, command.Date.Year, command.Date.Month));
            }
        }

        private void PlanBusinessToBusiness(AutoplanMonthChedulerCommand command, DAL.Models.Employees.Employee employee, List<MonthDay> daysInMonth)
        {
            var employeeDays = GetCleanedEmployeeDays(employee.Id, command.Date);
            var dayModuloOffset = GetDayModuloFromPreviousMonth(employee.Id, command.Date);
            foreach (var monthDay in daysInMonth)
            {
                var dayModulo = (dayModuloOffset + monthDay.Day) % 4;
                if (dayModulo < 2)
                {
                    var startDate = new DateTime(command.Date.Year, command.Date.Month, monthDay.Day);
                    var employeeDay = employeeDays.FirstOrDefault(x => x.Start.Day == monthDay.Day);

                    if (employeeDay == null || employeeDay.IsLeave == false)
                    {
                        if (employeeDay == null)
                        {
                            employeeDay = databaseCommand
                                .ExecuteCommand(new AddEmployeeDayDataCommand(employee.Id, startDate));
                        }

                        employeeDay.DayInfo = DayInfo.Work;

                        if (dayModulo == 0)
                        {
                            employeeDay.Start = startDate.AddHours(7);
                            employeeDay.End = startDate.AddHours(19);
                        }
                        else if (dayModulo == 1)
                        {
                            employeeDay.Start = startDate.AddHours(19);
                            employeeDay.End = startDate.AddDays(1).AddHours(7);
                        }
                        else
                        {
                            throw new Exception("Unknown day modulo");
                        }
                    }
                }
            }
        }

        private void PlanContractOfEmployment(AutoplanMonthChedulerCommand command, DAL.Models.Employees.Employee employee, List<MonthDay> daysInMonth)
        {
            var employeeDays = GetCleanedEmployeeDays(employee.Id, command.Date);

            var dayModuloOffset = GetDayModuloFromPreviousMonth(employee.Id, command.Date);
            foreach (var monthDay in daysInMonth)
            {
                var dayModulo = (dayModuloOffset + monthDay.Day) % 4;
                if (dayModulo < 2)
                {
                    var startDate = new DateTime(command.Date.Year, command.Date.Month, monthDay.Day);
                    var employeeDay = employeeDays.FirstOrDefault(x => x.Start.Day == monthDay.Day);

                    if (employeeDay == null || employeeDay.IsLeave == false)
                    {
                        if (employeeDay == null)
                        {
                            employeeDay = databaseCommand
                                .ExecuteCommand(new AddEmployeeDayDataCommand(employee.Id, startDate));
                        }

                        employeeDay.DayInfo = DayInfo.Work;

                        if (dayModulo == 0)
                        {
                            employeeDay.Start = startDate.AddHours(7);
                            employeeDay.End = startDate.AddHours(19);
                        }
                        else if (dayModulo == 1)
                        {
                            employeeDay.Start = startDate.AddHours(19);
                            employeeDay.End = startDate.AddDays(1).AddHours(7);
                        }
                        else
                        {
                            throw new Exception("Unknown day modulo");
                        }
                    }
                }
            }
        }

        private void PlanFixedContractOfEmployment(AutoplanMonthChedulerCommand command, DAL.Models.Employees.Employee employee, List<MonthDay> daysInMonth)
        {
            var employeeDays = GetCleanedEmployeeDays(employee.Id, command.Date);

            foreach (var monthDay in daysInMonth)
            {
                if (monthDay.IsHoliday == false && monthDay.DayOfWeek != DayOfWeek.Saturday)
                {
                    var startDate = new DateTime(command.Date.Year, command.Date.Month, monthDay.Day, 7, 0, 0);
                    var employeeDay = employeeDays.FirstOrDefault(x => x.Start.Day == monthDay.Day);

                    if (employeeDay == null || employeeDay.IsLeave == false)
                    {
                        if (employeeDay == null)
                        {
                            employeeDay = databaseCommand
                                .ExecuteCommand(new AddEmployeeDayDataCommand(employee.Id, startDate));
                        }

                        employeeDay.Start = startDate;
                        employeeDay.End = startDate.Date.AddHours(14).AddMinutes(35);
                        employeeDay.DayInfo = DayInfo.Work;
                    }
                }
            }
        }

        private List<EmployeeDay> GetCleanedEmployeeDays(int employeeId, DateTime date)
        {
            var employeeDays = databaseQuery
                .ExecuteQuery(new GetEmployeeDayForMonth(employeeId, date))
                .ToList();

            employeeDays.ForEach(employeeDay =>
            {
                if (!employeeDay.IsReadOnly)
                {
                    employeeDay.DayInfo = DayInfo.Undefined;
                }
            });

            return employeeDays;
        }

        private int GetDayModuloFromPreviousMonth(int employeeId, DateTime currentMonth)
        {
            var previousMonth = currentMonth.AddMonths(-1);
            previousMonth = new DateTime(previousMonth.Year, previousMonth.Month, 1);

            var daysInMonth = DateTime.DaysInMonth(previousMonth.Month, previousMonth.Day);

            var employeeDays = databaseQuery
                .ExecuteQuery(new GetEmployeeDayForMonth(employeeId, previousMonth))
                .ToList();

            var lastDayInMonth = employeeDays.FirstOrDefault(x => x.Start.Day == daysInMonth);
            if (lastDayInMonth != null && lastDayInMonth.DayInfo == DayInfo.Work)
            {
                if (lastDayInMonth.Start.Hour == 7)
                {
                    return 1;
                }

                if (lastDayInMonth.Start.Hour == 19)
                {
                    return 2;
                }
            }

            var previousDayInMonth = employeeDays.FirstOrDefault(x => x.Start.Day == daysInMonth - 1);
            if (previousDayInMonth != null && previousDayInMonth.DayInfo == DayInfo.Work)
            {
                if (previousDayInMonth.Start.Hour == 7)
                {
                    return 2;
                }

                if (previousDayInMonth.Start.Hour == 19)
                {
                    return 3;
                }
            }

            return 0;
        }
    }
}
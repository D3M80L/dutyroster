using System;
using System.Linq;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.Models.Employee;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models;
using DutyRoster.DAL.Models.EmployeeDays;
using DutyRoster.DAL.Models.Employees;
using DutyRoster.DAL.Queries;
using DutyRoster.DAL.Queries.Employees;
using DutyRoster.DAL.Queries.Schedulers;

namespace DutyRoster.BLL.Commands
{
    internal sealed class GetEmployeeWorkSummaryCommandHandler : ICommandHandler<GetEmployeeWorkSummaryCommand>
    {
        private readonly IDatabaseQuery databaseQuery;

        public GetEmployeeWorkSummaryCommandHandler(IDatabaseQuery databaseQuery)
        {
            this.databaseQuery = databaseQuery;
        }

        public void Handle(GetEmployeeWorkSummaryCommand command)
        {
            var startDate = new DateTime(command.Year, command.Month, 1);

            var employeeWorkPeriod = databaseQuery
                .ExecuteQuery(new GetEmployeeWorkPeriodsInMonth(command.EmployeeId, startDate))
                .SingleOrDefault();

            if (employeeWorkPeriod == null)
            {
                return;
            }

            var employeeWorkHoursSummary = new EmployeeWorkSummary();

            var employeeDays = databaseQuery
                .ExecuteQuery(new GetEmployeeDayForMonth(command.EmployeeId, command.Year, command.Month))
                .ToList();

            employeeWorkHoursSummary.HolidaysTaken = employeeDays
                .Count(x => x.DayInfo == DayInfo.Holiday);

            employeeWorkHoursSummary.Leaves.LeavesTaken = employeeDays.Count(x => x.IsLeave);
            employeeWorkHoursSummary.Leaves.LeavesDuringWork = employeeDays.Count(x => x.IsLeave && x.DayInfo == DayInfo.Work);
            employeeWorkHoursSummary.Leaves.LeaveToReduceFromWorkInMinutes = employeeDays
                .Where(x => x.IsLeave && x.DayInfo == DayInfo.Work)
                .Select(s => s.End - s.Start)
                .Sum(s => (int)s.TotalMinutes);

            if (employeeWorkPeriod.EmploymentType == EmploymentType.BusinessToBusiness)
            {
                var workingDays = employeeDays
                    .Where(x => x.DayInfo == DayInfo.Work);

                employeeWorkHoursSummary.MinutesSpent = workingDays
                    .Select(s => s.End - s.Start)
                    .Sum(s => (int)s.TotalMinutes);
            }

            if (employeeWorkPeriod.EmploymentType == EmploymentType.ContractOfEmployment)
            {
                var workingDays = employeeDays
                    .Where(x => x.DayInfo == DayInfo.Work || x.DayInfo == DayInfo.Holiday);

                var totalMinutes = workingDays
                    .Select(s =>
                    {
                        if (s.DayInfo == DayInfo.Holiday)
                        {
                            return 7 * 60 + 35;
                        }
                        return (s.End - s.Start).TotalMinutes;
                    })
                    .Sum(s => (int)s);

                employeeWorkHoursSummary.MinutesSpent = totalMinutes;
            }

            if (employeeWorkPeriod.EmploymentType == EmploymentType.FixedContractOfEmployment)
            {
                var workingDays = employeeDays
                    .Where(x => x.DayInfo == DayInfo.Work || x.DayInfo == DayInfo.Holiday);

                var totalMinutes = workingDays
                    .Select(s =>
                    {
                        if (s.DayInfo == DayInfo.Holiday)
                        {
                            return 7 * 60 + 35;
                        }
                        return (s.End - s.Start).TotalMinutes;
                    })
                    .Sum(s => (int)s);

                employeeWorkHoursSummary.MinutesSpent = totalMinutes;
            }

            command.Result = employeeWorkHoursSummary;
        }
    }
}
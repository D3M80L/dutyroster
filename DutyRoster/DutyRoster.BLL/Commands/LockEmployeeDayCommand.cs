﻿using System;
using DutyRoster.BLL.Infrastructure;

namespace DutyRoster.BLL.Commands
{
    public sealed class LockEmployeeDayCommand : Command
    {
        public LockEmployeeDayCommand(int employeeId, DateTime date, bool @lock)
        {
            EmployeeId = employeeId;
            Lock = @lock;
            Date = date.Date;
        }

        public int EmployeeId { get; }

        public DateTime Date { get; }
        public bool Lock { get; }
    }

    public sealed class LockEmployeeDay
    {
        public bool MakeReadOnly { get; set; }
    }
}

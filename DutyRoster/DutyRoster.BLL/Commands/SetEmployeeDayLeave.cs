﻿using System;
using DutyRoster.BLL.Infrastructure;

namespace DutyRoster.BLL.Commands
{
    public sealed class SetEmployeeDayLeaveCommand : Command
    {
        public SetEmployeeDayLeaveCommand(int employeeId, DateTime dateTime, bool hasLeave)
        {
            EmployeeId = employeeId;
            Date = dateTime;
            HasLeave = hasLeave;
        }

        public int EmployeeId { get; }

        public DateTime Date { get; }

        public bool HasLeave { get; }
    }

    public sealed class SetEmployeeDayLeave
    {
        public int EmployeeId { get; set; }

        public bool HasLeave { get; set; }
    }
}

﻿using System;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.DAL.Commands;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Queries;
using DutyRoster.DAL.Queries.Schedulers;

namespace DutyRoster.BLL.Commands
{
    internal sealed class LockEmployeeDayCommandHandler : ICommandHandler<LockEmployeeDayCommand>
    {
        private readonly IDatabaseQuery databaseQuery;
        private readonly IDatabaseCommand databaseCommand;
        private readonly IUnitOfWork unitOfWork;

        public LockEmployeeDayCommandHandler(IDatabaseQuery databaseQuery, IDatabaseCommand databaseCommand, IUnitOfWork unitOfWork)
        {
            this.databaseQuery = databaseQuery;
            this.databaseCommand = databaseCommand;
            this.unitOfWork = unitOfWork;
        }

        public void Handle(LockEmployeeDayCommand command)
        {
            var employeeDay = databaseQuery
                .ExecuteQuery(new GetEmployeeDay(command.EmployeeId, command.Date));

            if (employeeDay == null)
            {
                employeeDay = databaseCommand.ExecuteCommand(new AddEmployeeDayDataCommand(command.EmployeeId, command.Date));
            }

            employeeDay.IsReadOnly = command.Lock;

            unitOfWork.Save();
        }
    }
}
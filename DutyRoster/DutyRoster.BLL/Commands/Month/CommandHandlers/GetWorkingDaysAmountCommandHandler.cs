using System;
using System.Linq;
using DutyRoster.BLL.Commands.Month.Models;
using DutyRoster.BLL.Infrastructure;

namespace DutyRoster.BLL.Commands.Month.CommandHandlers
{
    internal sealed class GetWorkingDaysAmountCommandHandler : ICommandHandler<GetWorkingDaysAmountCommand>
    {
        private readonly ICommandBus commandBus;

        public GetWorkingDaysAmountCommandHandler(ICommandBus commandBus)
        {
            this.commandBus = commandBus;
        }

        public void Handle(GetWorkingDaysAmountCommand command)
        {
            var daysInMonth = commandBus.Execute(new GetDaysForMonthCommand(command.Year, command.Month)).Result;
            var workingDaysAmount = new WorkingDaysAmount();
            workingDaysAmount.TotalDays = daysInMonth.Count;
            workingDaysAmount.NonWorkingDays = daysInMonth
                .Where(x => x.IsHoliday || x.DayOfWeek == DayOfWeek.Saturday)
                .Select(s =>
                {
                    if (s.DayOfWeek == DayOfWeek.Saturday && s.IsHoliday)
                    {
                        return 2;
                    }

                    return 1;
                })
                .Sum();
            workingDaysAmount.WorkingDays = workingDaysAmount.TotalDays - workingDaysAmount.NonWorkingDays;
            workingDaysAmount.HolidaysInSaturdays = daysInMonth.Count(x => x.IsHoliday && x.DayOfWeek == DayOfWeek.Saturday);

            command.Result = workingDaysAmount;
        }
    }
}
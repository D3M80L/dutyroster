﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DutyRoster.BLL.Commands.Month.Models;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Queries.Schedulers;

namespace DutyRoster.BLL.Commands.Month.CommandHandlers
{
    internal sealed class GetEmployeeMonthSummaryCommandHandler : ICommandHandler<GetEmployeeMonthSummaryCommand>
    {
        private readonly IDatabaseQuery databaseQuery;
        private readonly ICommandBus commandBus;

        public GetEmployeeMonthSummaryCommandHandler(IDatabaseQuery databaseQuery, ICommandBus commandBus)
        {
            this.databaseQuery = databaseQuery;
            this.commandBus = commandBus;
        }

        public void Handle(GetEmployeeMonthSummaryCommand command)
        {
            var availableEmployees = databaseQuery
                .ExecuteQuery(new GetAvailableEmployeesInMonth(command.Year, command.Month))
                .ToList();

            var viewModel = new GetEmployeeMonthSummaryViewModel();
            var items = new List<EmployeeMonthItem>();
            viewModel.Items = items;

            foreach (var employee in availableEmployees)
            {
                var item = new EmployeeMonthItem();
                item.EmployeeId = employee.Id;
                item.FullName = employee.FirstName + " " + employee.LastName;
                var workSummary = commandBus.Execute(new GetEmployeeWorkSummaryCommand(employee.Id, command.Year, command.Month)).Result;

                item.Leaves = GetTimeString(workSummary.Leaves.LeavesDuringWork);
                item.Holidays = GetTimeString(workSummary.HolidaysTaken);
                item.Work = workSummary.MinutesSpent.ToString(CultureInfo.InvariantCulture);
                items.Add(item);
            }
            command.Result = viewModel;
        }

        private string GetTimeString(int amount)
        {
            if (amount == 0)
            {
                return "-";
            }
            var timespan = new TimeSpan(0, (7*60+35)*amount, 0);
            return string.Format("{0} ({1}h {2}m)", amount, (int)timespan.TotalHours, timespan.Minutes);
        }
    }
}
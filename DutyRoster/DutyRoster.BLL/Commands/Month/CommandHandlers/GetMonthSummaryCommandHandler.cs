﻿using System;
using System.Collections.Generic;
using System.Globalization;
using DutyRoster.BLL.Commands.Month.Models;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Queries.Schedulers;
using DutyRoster.DAL.Queries.Schedules;

namespace DutyRoster.BLL.Commands.Month.CommandHandlers
{
    internal sealed class GetMonthSummaryCommandHandler : ICommandHandler<GetMonthSummaryCommand>
    {
        private readonly ICommandBus commandBus;
        private readonly IDatabaseQuery databaseQuery;

        public GetMonthSummaryCommandHandler(ICommandBus commandBus, IDatabaseQuery databaseQuery)
        {
            this.commandBus = commandBus;
            this.databaseQuery = databaseQuery;
        }

        public void Handle(GetMonthSummaryCommand command)
        {
            var viewModel = new MonthSummaryViewModel();
            var items = new List<MonthSummaryItem>();
            viewModel.MonthName = new DateTime(command.Year, command.Month, 1).ToString("MMMM");
            viewModel.Year = command.Year;
            viewModel.Items = items;

            var monthSchedulerInfo = databaseQuery.ExecuteQuery(new GetMonthSchedulerInfo(command.Year, command.Month));
            items.Add(BuildSummaryItem("Jest zablokowany", monthSchedulerInfo.IsLocked));

            var workingDaysAmount = commandBus.Execute(new GetWorkingDaysAmountCommand(command.Year, command.Month)).Result;

            items.Add(BuildSummaryItem("Łącznie dni", workingDaysAmount.TotalDays));
            items.Add(BuildSummaryItem("Dni wolne", workingDaysAmount.NonWorkingDays));
            items.Add(BuildSummaryItem("Dni pracujące", workingDaysAmount.WorkingDays));
            items.Add(BuildSummaryItem("Święta do odbioru", workingDaysAmount.HolidaysInSaturdays));
            items.Add(BuildSummaryItem("7h 35min x dni pracujące", new TimeSpan(0, (7 * 60 + 35) * workingDaysAmount.WorkingDays, 0)));
            items.Add(BuildSummaryItem("8h 00min x dni pracujące", new TimeSpan(0, (8 * 60) * workingDaysAmount.WorkingDays, 0)));

            command.Result = viewModel;
        }

        private MonthSummaryItem BuildSummaryItem<TValue>(string title, TValue value)
            where TValue : IConvertible
        {
            return new MonthSummaryItem
            {
                Title = title,
                Description = value.ToString(CultureInfo.InvariantCulture)
            };
        }

        private MonthSummaryItem BuildSummaryItem(string title, TimeSpan value)
        {
            return new MonthSummaryItem
            {
                Title = title,
                Description = $"{(int)value.TotalHours}h {value.Minutes}m"
            };
        }
    }
}
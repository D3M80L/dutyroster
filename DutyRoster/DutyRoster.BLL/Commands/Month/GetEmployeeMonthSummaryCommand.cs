﻿using DutyRoster.BLL.Commands.Month.Models;
using DutyRoster.BLL.Infrastructure;

namespace DutyRoster.BLL.Commands.Month
{
    public sealed class GetEmployeeMonthSummaryCommand : Command<GetEmployeeMonthSummaryViewModel>
    {
        public GetEmployeeMonthSummaryCommand(int year, int month)
        {
            Year = year;
            Month = month;
        }

        public int Year { get; }

        public int Month { get; }
    }
}

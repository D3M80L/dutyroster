﻿namespace DutyRoster.BLL.Commands.Month.Models
{
    public sealed class MonthSummaryItem
    {
        public string Title { get; set; }

        public string Description { get; set; }
    }
}
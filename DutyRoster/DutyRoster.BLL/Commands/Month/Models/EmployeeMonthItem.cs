namespace DutyRoster.BLL.Commands.Month.Models
{
    public sealed class EmployeeMonthItem
    {
        public string FullName { get; set; }

        public int EmployeeId { get; set; }

        public string Leaves { get; set; }

        public string Holidays { get; set; }

        public string Work { get; set; }
    }
}
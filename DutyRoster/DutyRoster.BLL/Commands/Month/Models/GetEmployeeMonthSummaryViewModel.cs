using System.Collections.Generic;

namespace DutyRoster.BLL.Commands.Month.Models
{
    public sealed class GetEmployeeMonthSummaryViewModel
    {
        public IEnumerable<EmployeeMonthItem> Items { get; set; }
    }
}
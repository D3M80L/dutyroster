namespace DutyRoster.BLL.Commands.Month.Models
{
    public sealed class WorkingDaysAmount
    {
        public int TotalDays { get; set; }

        public int NonWorkingDays { get; set; }
        public int WorkingDays { get; set; }
        public int HolidaysInSaturdays { get; set; }
    }
}
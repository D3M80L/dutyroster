﻿using System.Collections.Generic;

namespace DutyRoster.BLL.Commands.Month.Models
{
    public sealed class MonthSummaryViewModel
    {
        public int Year { get; set; }

        public string MonthName { get; set; }

        public IEnumerable<MonthSummaryItem> Items { get; set; }
    }
}
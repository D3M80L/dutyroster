﻿using DutyRoster.BLL.Commands.Month.Models;
using DutyRoster.BLL.Infrastructure;

namespace DutyRoster.BLL.Commands.Month
{
    public sealed class GetWorkingDaysAmountCommand : Command<WorkingDaysAmount>
    {
        public GetWorkingDaysAmountCommand(int year, int month)
        {
            Year = year;
            Month = month;
        }

        public int Year { get; }

        public int Month { get; }
    }
}

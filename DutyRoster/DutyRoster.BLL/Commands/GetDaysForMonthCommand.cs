﻿using System.Collections.Generic;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.ViewModels.Scheduler.Models;

namespace DutyRoster.BLL.Commands
{
    public sealed class GetDaysForMonthCommand : Command<List<MonthDay>>
    {
        public GetDaysForMonthCommand(int year, int month)
        {
            Year = year;
            Month = month;
        }

        public int Year { get; }
        public int Month { get; }
    }
}

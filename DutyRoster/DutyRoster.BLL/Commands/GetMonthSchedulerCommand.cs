﻿using System;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.ViewModels.Scheduler;

namespace DutyRoster.BLL.Commands
{
    public sealed class GetMonthSchedulerCommand : Command<MonthScheduler>
    {
        public GetMonthSchedulerCommand(int year, int month)
        {
            Date = new DateTime(year, month, 1);
        }

        public GetMonthSchedulerCommand(DateTime date)
        {
            Date = date.Date;
        }

        public DateTime Date { get; }
    }
}
﻿using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.Models;

namespace DutyRoster.BLL.Commands
{
    public sealed class GetSettlementPeriodCommand : Command<SettlementPeriod>
    {
        public GetSettlementPeriodCommand(int year, int month)
        {
            Year = year;
            Month = month;
        }

        public int Year { get; }

        public int Month { get; }
    }
}

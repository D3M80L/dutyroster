﻿using DutyRoster.BLL.Commands.Schedule;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.DAL.Commands;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Queries;
using DutyRoster.DAL.Queries.Schedulers;

namespace DutyRoster.BLL.Commands
{
    internal sealed class SetEmployeeDayLeaveCommandHandler : ICommandHandler<SetEmployeeDayLeaveCommand>
    {
        private readonly IDatabaseQuery databaseQuery;
        private readonly IDatabaseCommand databaseCommand;
        private readonly IUnitOfWork unitOfWork;
        private readonly ICommandBus commandBus;

        public SetEmployeeDayLeaveCommandHandler(IDatabaseQuery databaseQuery, IUnitOfWork unitOfWork, IDatabaseCommand databaseCommand, ICommandBus commandBus)
        {
            this.databaseQuery = databaseQuery;
            this.unitOfWork = unitOfWork;
            this.databaseCommand = databaseCommand;
            this.commandBus = commandBus;
        }

        public void Handle(SetEmployeeDayLeaveCommand command)
        {
            var employeeDayQuery = databaseQuery
                .ExecuteQuery(new GetEmployeeDay(command.EmployeeId, command.Date));

            if (employeeDayQuery == null)
            {
                employeeDayQuery = databaseCommand
                    .ExecuteCommand(new AddEmployeeDayDataCommand(command.EmployeeId, command.Date));
            }

            employeeDayQuery.IsLeave = command.HasLeave;
            unitOfWork.Save();

            commandBus.Execute(new RecalculateEmployeeWorkForMonthCommand(command.EmployeeId, command.Date.Year, command.Date.Month));
        }
    }
}

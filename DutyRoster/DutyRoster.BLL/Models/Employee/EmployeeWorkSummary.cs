namespace DutyRoster.BLL.Models.Employee
{
    public sealed class EmployeeWorkSummary
    {
        public EmployeeWorkSummary()
        {
            Leaves = new LeavesSummary();
        }

        public int HolidaysTaken { get; set; }

        public int MinutesSpent { get; set; }

        public LeavesSummary Leaves { get; set; }

        public sealed class LeavesSummary
        {
            public int LeavesTaken { get; set; }

            public int LeavesDuringWork { get; set; }

            public int LeaveToReduceFromWorkInMinutes { get; set; }
        }
    }
}

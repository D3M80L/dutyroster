﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DutyRoster.BLL.Models.Employee
{
    public sealed class EmployeeSummaryViewModel
    {
        public int Id { get; set; }

        public string FullName { get; set; }
        public string Initials { get; set; }
        public string WorkType { get; set; }

        public EmployeeSummaryMonthViewModel MonthSummary { get; set; }
    }

    public sealed class EmployeeSummaryMonthViewModel
    {
        public IEnumerable<EmployeeSummaryItem> Items { get; set; }
        public DateTime Date { get; set; }
    }

    public sealed class EmployeeSummaryItem
    {
        public string Title { get; set; }

        public string Description { get; set; }
    }
}

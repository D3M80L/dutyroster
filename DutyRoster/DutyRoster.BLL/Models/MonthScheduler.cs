﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DutyRoster.BLL.Commands;
using DutyRoster.BLL.ViewModels.Scheduler.Models;

namespace DutyRoster.BLL.Models
{
    public sealed class MonthWorkHours
    {
        public string Title { get; set; }
        public int Hours { get; set; }

        public int Minutes { get; set; }
    }

    public sealed class SettlementPeriod
    {
        public IEnumerable<MonthInfo> Months { get; set; }

        public int TotalWorkingDays { get; set; }

        public int TotalDays { get; set; }
        public IEnumerable<MonthWorkHours> TotalWorkHours { get; internal set; }
    }
}

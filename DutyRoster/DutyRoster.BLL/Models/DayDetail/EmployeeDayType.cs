namespace DutyRoster.BLL.Models.DayDetail
{
    public enum EmployeeDayType
    {
        Unknown = 0,

        Work = 1,

        Holiday = 2,

        Leave = 3
    }
}
using System;
using System.Collections.Generic;
using DutyRoster.BLL.Commands;
using DutyRoster.BLL.Commands.DayDetail;
using DutyRoster.BLL.Commands.DayDetail.Models;
using DutyRoster.BLL.DataContracts;

namespace DutyRoster.BLL.Models.DayDetail
{
    public sealed class EmployeeDayDetail : IEmployeeId
    {
        public DateTime Date { get; set; }

        public string MonthName { get; set; }

        public int EmployeeId { get; set; }

        public string FullName { get; set; }

        public bool IsFixed { get; set; }

        public bool HasLeave { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public string Description { get; set; }

        public EmployeeDayType DayType { get; set; }

        public IEnumerable<OptionItem> DayTypes { get; set; }

        public bool IsLeave { get; set; }
    }
}
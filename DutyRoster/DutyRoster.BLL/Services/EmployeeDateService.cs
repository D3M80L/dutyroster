﻿using System;

namespace DutyRoster.BLL.Services
{
    internal sealed class EmployeeDateService
    {
        public static DateTime GetEndDate(DateTime startDate, DateTime endDate)
        {
            var startTime = startDate.TimeOfDay;
            var endTime = endDate.TimeOfDay;

            if (endTime <= startTime)
            {
                return startDate.Date
                    .AddDays(1)
                    .AddHours(endDate.Hour)
                    .AddMinutes(endDate.Minute);
            }

            return new DateTime(startDate.Year, startDate.Month, startDate.Day, endDate.Hour, endDate.Minute, 0);
        }
    }
}

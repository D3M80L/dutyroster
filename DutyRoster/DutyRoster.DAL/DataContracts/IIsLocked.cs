﻿namespace DutyRoster.DAL.DataContracts
{
    public interface IIsLocked
    {
        bool IsLocked { get; set; }
    }
}

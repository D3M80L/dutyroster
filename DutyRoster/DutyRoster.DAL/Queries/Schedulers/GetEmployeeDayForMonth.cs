﻿using System;
using System.Linq;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models;
using DutyRoster.DAL.Models.EmployeeDays;

namespace DutyRoster.DAL.Queries.Schedulers
{
    internal sealed class GetEmployeeDayForMonth : QueryResultBase<EmployeeDay>
    {
        public GetEmployeeDayForMonth(int employeeId, int year, int month)
        {
            EmployeeId = employeeId;
            Year = year;
            Month = month;
        }

        public GetEmployeeDayForMonth(int employeeId, DateTime date) : this(employeeId, date.Year, date.Month)
        {
        }

        public int EmployeeId { get; }

        public int Year { get; }

        public int Month { get; }

        internal override IQueryable<EmployeeDay> OnExecute(IDatabaseContext databaseContext)
        {
            return databaseContext.EmployeeDay
                .Where(x => x.EmployeeId == EmployeeId)
                .Where(x => x.Start.Year == Year && x.Start.Month == Month);
        }
    }
}
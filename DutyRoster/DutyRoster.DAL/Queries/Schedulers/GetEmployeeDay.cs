﻿using System;
using System.Linq;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models.EmployeeDays;

namespace DutyRoster.DAL.Queries.Schedulers
{
    internal sealed class GetEmployeeDay : QueryItemBase<EmployeeDay>
    {
        public GetEmployeeDay(int employeeId, DateTime date)
        {
            EmployeeId = employeeId;
            Date = date;
        }

        public int EmployeeId { get; }

        public DateTime Date { get; }

        internal override EmployeeDay OnExecute(IDatabaseContext databaseContext)
        {
            return databaseContext.EmployeeDay
                .Where(x => x.EmployeeId == EmployeeId)
                .FirstOrDefault(x => x.Start.Year == Date.Year && x.Start.Month == Date.Month && x.Start.Day == Date.Day);
        }
    }
}
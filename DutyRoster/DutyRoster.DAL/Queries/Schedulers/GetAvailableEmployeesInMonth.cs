﻿using System;
using System.Data.Entity;
using System.Linq;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models.Employees;

namespace DutyRoster.DAL.Queries.Schedulers
{
    internal sealed class GetAvailableEmployeesInMonth : QueryResultBase<Employee>
    {
        public GetAvailableEmployeesInMonth(int year, int month)
        {
            Year = year;
            Month = month;
        }

        public int Year { get; }
        public int Month { get; }

        internal override IQueryable<Employee> OnExecute(IDatabaseContext databaseContext)
        {
            var firstDayInMonth = new DateTime(Year, Month, 1);
            var firstDayInNextMonth = firstDayInMonth.AddMonths(1);

            var availableEmployees = databaseContext.EmployeeWorkPeriod
                .Where(x => x.Start < firstDayInNextMonth && (x.End == null || x.End > firstDayInMonth))
                .Select(s => s.Employee)
                .Include(i => i.WorkPeriod)
                .Distinct();

            return availableEmployees;
        }
    }
}
﻿using System;
using System.Data.Entity;
using System.Linq;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models.Employees;

namespace DutyRoster.DAL.Queries.Schedulers
{
    internal sealed class GetEmployeeAvailableInMonth : QueryItemBase<Employee>
    {
        public GetEmployeeAvailableInMonth(int employeeId, DateTime date)
        {
            EmployeeId = employeeId;
            Date = date.Date;
        }

        public int EmployeeId { get; }

        public DateTime Date { get; }

        internal override Employee OnExecute(IDatabaseContext databaseContext)
        {
            var employee = databaseContext.EmployeeWorkPeriod
                .Include(i => i.Employee)
                .Where(x => x.EmployeeId == EmployeeId)
                .Where(x => x.Start < Date && (x.End == null || x.End > Date))
                .Select(x => x.Employee)
                .SingleOrDefault();

            return employee;
        }
    }
}
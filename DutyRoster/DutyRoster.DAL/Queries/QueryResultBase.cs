﻿using System.Linq;
using DutyRoster.DAL.Infrastructure;

namespace DutyRoster.DAL.Queries
{
    public abstract class QueryResultBase<T> : QueryBase
        where T : class
    {
        public IQueryable<T> QueryResult { get; protected set; }

        internal override sealed void Execute(IDatabaseContext databaseContext)
        {
            QueryResult = OnExecute(databaseContext);
        }

        internal abstract IQueryable<T> OnExecute(IDatabaseContext databaseContext);
    }
}
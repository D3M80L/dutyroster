﻿using DutyRoster.DAL.Infrastructure;

namespace DutyRoster.DAL.Queries
{
    public abstract class QueryBase
    {
        internal abstract void Execute(IDatabaseContext databaseContext);
    }
}
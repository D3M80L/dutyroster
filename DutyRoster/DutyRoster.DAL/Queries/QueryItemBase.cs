﻿using DutyRoster.DAL.Implementations;
using DutyRoster.DAL.Infrastructure;

namespace DutyRoster.DAL.Queries
{
    public abstract class QueryItemBase<T> : QueryBase
    {
        public T QueryResult { get; protected set; }

        internal override sealed void Execute(IDatabaseContext databaseContext)
        {
            QueryResult = OnExecute(databaseContext);
        }

        internal abstract T OnExecute(IDatabaseContext databaseContext);
    }
}
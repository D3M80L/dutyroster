﻿using System;
using System.Data.Entity;
using System.Linq;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models.Employees;

namespace DutyRoster.DAL.Queries.Employees
{
    internal sealed class GetEmployeeWorkPeriodsInMonth : QueryResultBase<EmployeeWorkPeriod>
    {
        public GetEmployeeWorkPeriodsInMonth(int employeeId, DateTime date)
        {
            EmployeeId = employeeId;
            Date = date;
        }

        public int EmployeeId { get; }

        public DateTime Date { get; }

        internal override IQueryable<EmployeeWorkPeriod> OnExecute(IDatabaseContext databaseContext)
        {
            var startDate = new DateTime(Date.Year, Date.Month, 1);
            var nextMonth = startDate.AddMonths(1);

            return  databaseContext.EmployeeWorkPeriod
                .Include(i => i.Employee)
                .Where(x => x.EmployeeId == EmployeeId)
                .Where(x => x.Start <= nextMonth && (x.End == null || x.End >= startDate));
        }
    }
}

using System.Data.Entity;
using System.Linq;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models;
using DutyRoster.DAL.Models.Employees;

namespace DutyRoster.DAL.Queries.Employees
{
    internal sealed class GetEmployees : QueryResultBase<Employee>
    {
        internal override IQueryable<Employee> OnExecute(IDatabaseContext databaseContext)
        {
            return databaseContext.Employee
                .Include(i => i.WorkPeriod);
        }
    }
}
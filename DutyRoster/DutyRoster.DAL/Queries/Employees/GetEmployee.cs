﻿using System.Data.Entity;
using System.Linq;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models;
using DutyRoster.DAL.Models.Employees;

namespace DutyRoster.DAL.Queries.Employees
{
    internal sealed class GetEmployee : QueryItemBase<Employee>
    {
        public GetEmployee(int id)
        {
            Id = id;
        }

        public int Id { get; }

        internal override Employee OnExecute(IDatabaseContext databaseContext)
        {
            return databaseContext.Employee
                .Include(i => i.WorkPeriod)
                .FirstOrDefault(x => x.Id == Id);
        }
    }
}

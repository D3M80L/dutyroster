﻿using System.Linq;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models;

namespace DutyRoster.DAL.Queries
{
    internal sealed class GetBankHolidaysForMonthQuery : QueryResultBase<BankHoliday>
    {
        public GetBankHolidaysForMonthQuery(int year, int month)
        {
            Year = year;
            Month = month;
        }

        public int Year { get; }
        public int Month { get; }

        internal override IQueryable<BankHoliday> OnExecute(IDatabaseContext databaseContext)
        {
            return databaseContext.BankHoliday
                .Where(x => x.Year == null || x.Year == Year)
                .Where(x => x.Month == Month);
        }
    }
}
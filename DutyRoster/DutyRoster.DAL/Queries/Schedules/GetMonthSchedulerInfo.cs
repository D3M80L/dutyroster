﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models.Schedules;

namespace DutyRoster.DAL.Queries.Schedules
{
    internal sealed class GetMonthSchedulerInfo : QueryItemBase<MonthSchedulerInfo>
    {
        public GetMonthSchedulerInfo(int year, int month)
        {
            Year = year;
            Month = month;
        }

        public int Year { get; }

        public int Month { get; }

        internal override MonthSchedulerInfo OnExecute(IDatabaseContext databaseContext)
        {
            var monthSchedulerInfo = databaseContext.MonthSchedulerInfo
                .Local
                .FirstOrDefault(x => x.Year == Year && x.Month == Month);

            if (monthSchedulerInfo == null)
            {
                monthSchedulerInfo = databaseContext.MonthSchedulerInfo
                    .Include(i => i.EmployeeMonthSummary)
                    .FirstOrDefault(x => x.Year == Year && x.Month == Month);

                if (monthSchedulerInfo == null)
                {
                    monthSchedulerInfo = databaseContext.MonthSchedulerInfo.Create();
                    monthSchedulerInfo.Year = Year;
                    monthSchedulerInfo.Month = Month;
                    monthSchedulerInfo.EmployeeMonthSummary = new List<EmployeeMonthSummary>();
                    databaseContext.MonthSchedulerInfo.Add(monthSchedulerInfo);
                }
            }

            return monthSchedulerInfo;
        }
    }
}

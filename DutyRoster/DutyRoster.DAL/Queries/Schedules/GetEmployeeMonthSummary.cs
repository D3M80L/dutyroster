﻿using System.Linq;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models.Schedules;

namespace DutyRoster.DAL.Queries.Schedules
{
    internal sealed class GetEmployeeMonthSummary : QueryItemBase<EmployeeMonthSummary>
    {
        public GetEmployeeMonthSummary(int employeeId, int year, int month)
        {
            EmployeeId = employeeId;
            Year = year;
            Month = month;
        }

        public int EmployeeId { get; }

        public int Year { get; }

        public int Month { get; }

        internal override EmployeeMonthSummary OnExecute(IDatabaseContext databaseContext)
        {
            var monthSchedulerInfo = databaseContext.ExecuteQuery(new GetMonthSchedulerInfo(Year, Month));

            var employeeMonthSummary = monthSchedulerInfo
                .EmployeeMonthSummary
                .FirstOrDefault(x => x.EmployeeId == EmployeeId && x.Year == Year && x.Month == Month);

            if (employeeMonthSummary == null)
            {
                employeeMonthSummary = databaseContext.EmployeeMonthSummary.Create();
                employeeMonthSummary.EmployeeId = EmployeeId;
                employeeMonthSummary.Year = Year;
                employeeMonthSummary.Month = Month;
                monthSchedulerInfo.EmployeeMonthSummary.Add(employeeMonthSummary);
                databaseContext.EmployeeMonthSummary.Add(employeeMonthSummary);
            }

            return employeeMonthSummary;
        }
    }
}

﻿using System;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models;
using DutyRoster.DAL.Models.EmployeeDays;

namespace DutyRoster.DAL.Commands
{
    internal  sealed class AddEmployeeDayDataCommand : DataCommandBase<EmployeeDay>
    {
        public AddEmployeeDayDataCommand(int employeeId, DateTime date)
        {
            Date = date.Date;
            EmployeeId = employeeId;
        }

        public int EmployeeId { get; }

        public DateTime Date { get; }

        internal override EmployeeDay OnExecute(IDatabaseContext databaseContext)
        {
            var employeeWork = new EmployeeDay();
            employeeWork.EmployeeId = EmployeeId;
            employeeWork.Start = Date;
            employeeWork.End = Date.AddDays(1).AddSeconds(-1);
            databaseContext.EmployeeDay.Add(employeeWork);

            return employeeWork;
        }
    }
}

﻿using System;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models;
using DutyRoster.DAL.Models.Employees;

namespace DutyRoster.DAL.Commands
{
    internal sealed class AddEmployeeDataCommand : DataCommandBase<Employee>
    {
        public AddEmployeeDataCommand(Employee employee)
        {
            Employee = employee;
        }

        public Employee Employee { get; }

        internal override Employee OnExecute(IDatabaseContext databaseContext)
        {
            databaseContext.Employee.Add(Employee);
            return Employee;
        }
    }
}

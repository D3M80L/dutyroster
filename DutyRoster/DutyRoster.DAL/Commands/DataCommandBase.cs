using DutyRoster.DAL.Infrastructure;

namespace DutyRoster.DAL.Commands
{
    public abstract class DataCommandBase<TEntity>
        where TEntity: class
    {
        public TEntity Result { get; protected set; }

        internal void Execute(IDatabaseContext databaseContext)
        {
            Result = OnExecute(databaseContext);
        }

        internal abstract TEntity OnExecute(IDatabaseContext databaseContext);
    }
}
using DutyRoster.DAL.Commands;

namespace DutyRoster.DAL.Infrastructure
{
    public interface IDatabaseCommand
    {
        TEntity ExecuteCommand<TEntity>(DataCommandBase<TEntity> dataCommand)
            where TEntity : class;
    }
}
namespace DutyRoster.DAL.Infrastructure
{
    public interface IUnitOfWork
    {
        void Save();
    }
}
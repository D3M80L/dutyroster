using System.Data.Entity;
using DutyRoster.DAL.Models;
using DutyRoster.DAL.Models.EmployeeDays;
using DutyRoster.DAL.Models.Employees;
using DutyRoster.DAL.Models.Schedules;

namespace DutyRoster.DAL.Infrastructure
{
    internal interface IDatabaseContext : IDatabaseQuery, IDatabaseCommand, IUnitOfWork
    {
        IDbSet<Employee> Employee { get; set; }

        IDbSet<EmployeeWorkPeriod> EmployeeWorkPeriod { get; set; }

        IDbSet<EmployeeDay> EmployeeDay { get; set; }

        IDbSet<BankHoliday> BankHoliday { get; set; }

        IDbSet<MonthSchedulerInfo> MonthSchedulerInfo { get; set; }

        IDbSet<EmployeeMonthSummary> EmployeeMonthSummary { get; set; }
    }
}
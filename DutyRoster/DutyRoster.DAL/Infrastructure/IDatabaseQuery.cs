using System.Linq;
using DutyRoster.DAL.Queries;

namespace DutyRoster.DAL.Infrastructure
{
    public interface IDatabaseQuery
    {
        IQueryable<T> ExecuteQuery<T>(QueryResultBase<T> queryResultBase)
            where T : class;

        T ExecuteQuery<T>(QueryItemBase<T> queryItemBase)
            where T : class;
    }
}
using System;
using System.Data.Entity;
using DutyRoster.DAL.Models;
using DutyRoster.DAL.Models.EmployeeDays;
using DutyRoster.DAL.Models.Employees;

namespace DutyRoster.DAL.Implementations
{
    internal sealed class DatabaseInitializer : DropCreateDatabaseIfModelChanges<DatabaseContext>
    {
        protected override void Seed(DatabaseContext context)
        {
            for (int i = 0; i < 3; ++i)
            {
                var employeeWorkPeriod = new EmployeeWorkPeriod
                {
                    Start = DateTime.Now.AddMonths(-1),
                    End = DateTime.Now.AddMonths(i + 1),
                    EmploymentType = (EmploymentType)(i % 3),
                };

                if (employeeWorkPeriod.EmploymentType == EmploymentType.BusinessToBusiness)
                {
                    employeeWorkPeriod.MinumumWorkPerMonthInMinutes = 180*60;
                }

                var employee = new Employee
                {
                    Initials = string.Format("IM{0}", i),
                    FirstName = "Imi�" + i,
                    LastName = "Nazwisko" + i,
                };
                context.Employee.Add(employee);
                employeeWorkPeriod.Employee = employee;
                context.EmployeeWorkPeriod.Add(employeeWorkPeriod);

                var employeeDay = new EmployeeDay
                {
                    Employee = employee,
                    Start = DateTime.Now.Date.AddHours(7),
                    End = DateTime.Now.Date.AddHours(19),
                    DayInfo = DayInfo.Work,
                };
                context.EmployeeDay.Add(employeeDay);
            }

            context.BankHoliday.Add(new BankHoliday { Month = 5, Day = 1, Note = "�wi�to pracy" });
            context.BankHoliday.Add(new BankHoliday { Month = 5, Day = 3, Note = "Konstytucja 3go Maja" });
            context.BankHoliday.Add(new BankHoliday { Month = 8, Day = 15, Note = "Wniebowst�pienie NMP" });
            context.BankHoliday.Add(new BankHoliday { Month = 12, Day = 25, Note = "Bo�e Narodzenie" });
            context.BankHoliday.Add(new BankHoliday { Month = 12, Day = 26, Note = "Bo�e Narodzenie" });
            context.BankHoliday.Add(new BankHoliday { Month = 11, Day = 11, Note = "Dzie� Niepodleg�o�ci" });
            context.BankHoliday.Add(new BankHoliday { Month = 1, Day = 1, Note = "Nowy Rok" });
            context.BankHoliday.Add(new BankHoliday { Month = 1, Day = 6, Note = "Trzech Kr�li" });

            context.BankHoliday.Add(new BankHoliday { Month = 3, Day = 27, Year = 2016, Note = "Wielkanoc" });
            context.BankHoliday.Add(new BankHoliday { Month = 3, Day = 28, Year = 2016, Note = "Poniedzia�ek Wielkanocny" });
            context.BankHoliday.Add(new BankHoliday { Month = 5, Day = 15, Year = 2016, Note = "Zielone �wi�tki" });
            context.BankHoliday.Add(new BankHoliday { Month = 6, Day = 26, Year = 2016, Note = "Bo�e Cia�o" });

            base.Seed(context);
        }
    }
}
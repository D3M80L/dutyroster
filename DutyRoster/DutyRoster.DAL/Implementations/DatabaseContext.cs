﻿using System;
using System.Data.Entity;
using System.Linq;
using DutyRoster.DAL.Commands;
using DutyRoster.DAL.Infrastructure;
using DutyRoster.DAL.Models;
using DutyRoster.DAL.Models.EmployeeDays;
using DutyRoster.DAL.Models.Employees;
using DutyRoster.DAL.Models.Schedules;
using DutyRoster.DAL.Queries;

namespace DutyRoster.DAL.Implementations
{
    internal sealed class DatabaseContext : DbContext, IDatabaseContext
    {
        public DatabaseContext() : base("DutyRoster")
        {
            Database.SetInitializer(new DatabaseInitializer());
        }

        public IDbSet<Employee> Employee { get; set; }

        public IDbSet<EmployeeDay> EmployeeDay { get; set; }

        public IDbSet<EmployeeWorkPeriod> EmployeeWorkPeriod { get; set; }

        public IDbSet<BankHoliday> BankHoliday { get; set; }

        public IDbSet<MonthSchedulerInfo> MonthSchedulerInfo { get; set; }

        public IDbSet<EmployeeMonthSummary> EmployeeMonthSummary { get; set; }

        public void Save()
        {
            SaveChanges();
        }

        public IQueryable<T> ExecuteQuery<T>(QueryResultBase<T> queryResultBase) where T : class
        {
            queryResultBase.Execute(this);
            return queryResultBase.QueryResult;
        }

        public T ExecuteQuery<T>(QueryItemBase<T> queryItemBase) where T : class
        {
            queryItemBase.Execute(this);
            return queryItemBase.QueryResult;
        }

        public TEntity ExecuteCommand<TEntity>(DataCommandBase<TEntity> dataCommand)
            where TEntity: class
        {
            dataCommand.Execute(this);
            return dataCommand.Result;
        }
    }
}

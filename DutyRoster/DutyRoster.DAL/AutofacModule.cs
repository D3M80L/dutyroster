﻿using Autofac;
using DutyRoster.DAL.Implementations;
using DutyRoster.DAL.Infrastructure;

namespace DutyRoster.DAL
{
    public sealed class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DatabaseContext>()
                .As<IDatabaseContext>()
                .As<IDatabaseQuery>()
                .As<IDatabaseCommand>()
                .As<IUnitOfWork>()
                .InstancePerLifetimeScope();

            base.Load(builder);
        }
    }
}

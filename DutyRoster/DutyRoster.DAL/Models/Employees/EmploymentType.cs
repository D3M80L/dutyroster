﻿namespace DutyRoster.DAL.Models.Employees
{
    internal enum EmploymentType
    {
        ContractOfEmployment = 0,
        FixedContractOfEmployment = 1,
        BusinessToBusiness = 2
    }
}
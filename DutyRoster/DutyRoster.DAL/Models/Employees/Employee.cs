﻿using System.Collections.Generic;
using DutyRoster.DAL.Models.EmployeeDays;
using DutyRoster.DAL.Models.Schedules;

namespace DutyRoster.DAL.Models.Employees
{
    internal class Employee
    {
        public int Id { get; set; }

        public string Initials { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public virtual ICollection<EmployeeDay> EmployeeDayInfos { get; set; }

        public virtual EmployeeWorkPeriod WorkPeriod { get; set; }

        public virtual ICollection<EmployeeMonthSummary> EmployeeMonthSummaries { get; set; }
    }
}

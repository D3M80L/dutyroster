﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DutyRoster.DAL.Models.Employees
{
    internal class EmployeeWorkPeriod
    {
        [Key, ForeignKey("Employee")]
        public int EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }

        public DateTime Start { get; set; }

        public DateTime? End { get; set; }

        public EmploymentType EmploymentType { get; set; }

        public int? MinumumWorkPerMonthInMinutes { get; set; }
    }
}
﻿namespace DutyRoster.DAL.Models
{
    internal class BankHoliday
    {
        public int Id { get; set; }

        public int? Year { get; set; }

        public int Month { get; set; }

        public int Day { get; set; }

        public string Note { get; set; }
    }
}
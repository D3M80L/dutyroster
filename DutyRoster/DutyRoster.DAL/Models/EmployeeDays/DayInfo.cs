﻿namespace DutyRoster.DAL.Models.EmployeeDays
{
    internal enum DayInfo
    {
        Undefined = 0,

        Work = 1,

        Holiday = 2
    }
}
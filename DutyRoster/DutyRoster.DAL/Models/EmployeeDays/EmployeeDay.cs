﻿using System;
using DutyRoster.DAL.Models.Employees;

namespace DutyRoster.DAL.Models.EmployeeDays
{
    internal class EmployeeDay
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public DayInfo DayInfo { get; set; }

        public bool IsReadOnly { get; set; }

        public bool IsLeave { get; set; }

        public string Note { get; set; }
    }
}
﻿using DutyRoster.DAL.DataContracts;

namespace DutyRoster.DAL.Models.Schedules
{
    internal class EmployeeMonthSummary : IIsLocked
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public int Year { get; set; }

        public int Month { get; set; }

        public int HolidaysTaken { get; set; }

        public int LeavesTaken { get; set; }

        public int LeavesLenght { get; set; }

        public int WorkInMinutes { get; set; }

        public int WorkFromPreviousMonth { get; set; }

        public int WorkForNextMonth { get; set; }

        public int MinimumWork { get; set; }

        public bool IsLocked { get; set; }

        public int MonthSchedulerInfoId { get; set; }

        public virtual MonthSchedulerInfo MonthSchedulerInfo { get; set; }
    }
}
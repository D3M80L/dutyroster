﻿using System.Collections.Generic;

namespace DutyRoster.DAL.Models.Schedules
{
    internal class MonthSchedulerInfo
    {
        public int Id { get; set; }

        public int Year { get; set; }

        public int Month { get; set; }

        public bool IsLocked { get; set; }

        public virtual ICollection<EmployeeMonthSummary> EmployeeMonthSummary { get; set; }
    }
}
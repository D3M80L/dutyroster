﻿using System.Web.Http;
using DutyRoster.BLL.Commands.Employee;
using DutyRoster.BLL.Commands.Employee.Models;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.Models.Employee;

namespace DutyRoster.Facing.Api
{
    public sealed class EmployeeController : ApiController
    {
        private readonly ICommandBus commandBus;

        public EmployeeController(ICommandBus commandBus)
        {
            this.commandBus = commandBus;
        }

        [Route("api/employee")]
        public EmployeeListViewModel Get()
        {
            return commandBus.Execute(new GetEmployeeListViewModelCommand()).Result;
        }

        [Route("api/employee/{id}")]
        public EmployeeViewModel Get(int id)
        {
            return commandBus.Execute(new GetEmployeeViewModelCommand(id)).Result;
        }

        [HttpPost]
        [Route("api/employee/{id}")]
        public void Post(int id, EmployeeViewModel model)
        {
            commandBus.Execute(new UpdateEmployeeViewModelCommand(id, model));
        }

        [Route("api/employee/{id}/{summary}")]
        public EmployeeSummaryViewModel GetSummary(int id)
        {
            return commandBus.Execute(new GetEmployeeSummaryCommand(id)).Result;
        }

        [HttpPost]
        [Route("api/employee/add")]
        public void Post(EmployeeViewModel model)
        {
            commandBus.Execute(new CreateEmployeeViewModelCommand(model));
        }
    }
}

using System.Web.Http;
using DutyRoster.BLL.Commands.Month;
using DutyRoster.BLL.Commands.Month.Models;
using DutyRoster.BLL.Infrastructure;

namespace DutyRoster.Facing.Api
{
    public sealed class MonthController : ApiController
    {
        private readonly ICommandBus commandBus;

        public MonthController(ICommandBus commandBus)
        {
            this.commandBus = commandBus;
        }

        [HttpGet]
        [Route("api/month/{month}/{year}/summary")]
        public MonthSummaryViewModel SummaryGet(int year, int month)
        {
            
            return commandBus.Execute(new GetMonthSummaryCommand(year, month)).Result;
        }

        [HttpGet]
        [Route("api/month/{month}/{year}/employeesummary")]
        public GetEmployeeMonthSummaryViewModel EmployeeSummaryGet(int year, int month)
        {
            return commandBus.Execute(new GetEmployeeMonthSummaryCommand(year, month)).Result;
        }
    }
}
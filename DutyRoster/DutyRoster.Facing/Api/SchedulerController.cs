﻿using System;
using System.Web.Http;
using DutyRoster.BLL.Commands;
using DutyRoster.BLL.Commands.DayDetail;
using DutyRoster.BLL.Commands.Schedule;
using DutyRoster.BLL.Infrastructure;
using DutyRoster.BLL.Models;
using DutyRoster.BLL.Models.DayDetail;
using DutyRoster.BLL.ViewModels.Scheduler;
using DutyRoster.BLL.ViewModels.Scheduler.Models;

namespace DutyRoster.Facing.Api
{
    public class SchedulerController : ApiController
    {
        private readonly ICommandBus commandBus;

        public SchedulerController(ICommandBus commandBus)
        {
            this.commandBus = commandBus;
        }

        [HttpGet]
        public MonthScheduler Get()
        {
            return commandBus.Execute(new GetMonthSchedulerCommand(DateTime.Now)).Result;
        }

        [HttpGet]
        [Route("api/scheduler/{year}/{month}")]
        public MonthScheduler Get(int year, int month)
        {
            return commandBus.Execute(new GetMonthSchedulerCommand(year, month)).Result;
        }

        [HttpPost]
        [Route("api/scheduler/{year}/{month}/autoplan")]
        public void AutoplanPost(int year, int month)
        {
            commandBus.Execute(new AutoplanMonthChedulerCommand(year, month));
        }

        [HttpPost]
        [Route("api/scheduler/{year}/{month}/lock")]
        public void LockMonthPost(int year, int month)
        {
            commandBus.Execute(new LockSchedulerMonthCommand(year, month));
        }

        [HttpPost]
        [Route("api/scheduler/{year}/{month}/clean")]
        public void CleanPons(int year, int month)
        {
            commandBus.Execute(new CleanMonthSchedulerCommand(year, month));
        }

        [HttpGet]
        [Route("api/scheduler/{year}/{month}/settlement")]
        public SettlementPeriod SettlementGet(int year, int month)
        {
            return commandBus.Execute(new GetSettlementPeriodCommand(year, month)).Result;
        }

        [HttpPost]
        [Route("api/scheduler/{year}/{month}/{day}/employee/{employeeId}/fixday")]
        public void FixPost(int year, int month, int day, int employeeId, LockEmployeeDay model)
        {
            commandBus.Execute(new LockEmployeeDayCommand(employeeId, new DateTime(year, month, day), model.MakeReadOnly));
        }

        [HttpPost]
        [Route("api/scheduler/{year}/{month}/{day}/employee/{employeeId}/leave")]
        public void Leave(int year, int month, int day, int employeeId, SetEmployeeDayLeave model)
        {
            commandBus.Execute(new SetEmployeeDayLeaveCommand(employeeId, new DateTime(year, month, day), model.HasLeave));
        }

        [Route("api/scheduler/{year}/{month}/{day}/employee/{employeeId}")]
        [HttpPost]
        public void SetEmployeeInfoPost(int year, int month, int day, int employeeId, EmployeeSchedulerDayInfo model)
        {
            model.Year = year;
            model.Month = month;
            model.Day = day;
            model.EmployeeId = employeeId;
            commandBus.Execute(new SetEmployeeSchedulerDayInfoCommand(model));
        }

        [Route("api/scheduler/{year}/{month}/{day}/employee/{employeeId}/dayinfo")]
        [HttpGet]
        public EmployeeDayDetail GetEmployeeDayInfo(int year, int month, int day, int employeeId)
        {
            return commandBus.Execute(new GetEmployeeDayDetailCommand(employeeId, year, month, day)).Result;
        }

        [Route("api/scheduler/{year}/{month}/{day}/employee/{employeeId}/dayinfo")]
        [HttpPost]
        public void SetEmployeeDayInfo(int year, int month, int day, int employeeId, EmployeeDayDetail model)
        {
            model.Date = new DateTime(year, month, day);
            model.EmployeeId = employeeId;

            commandBus.Execute(new SetEmployeeDayDetailCommand(model));
        }

        [Route("api/scheduler/{year}/{month}/worksummary")]
        [HttpGet]
        public MonthWorkSummaryViewModel WorkSummaryGet(int year, int month)
        {
            return commandBus.Execute(new GetMonthWorkSummaryCommand(year, month)).Result;
        }

        [Route("api/scheduler/{year}/{month}/worksummary")]
        [HttpPost]
        public void WorkSummarySet(int year, int month, EmployeesMonthWorkSummary model)
        {
            commandBus.Execute(new SetEmployeesMonthWorkSummaryCommand(year, month, model));
        }
    }
}

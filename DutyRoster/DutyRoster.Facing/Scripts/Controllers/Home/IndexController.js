﻿var myApp = angular.module('myApp', ['ui.bootstrap', 'ngRoute', 'dialogs.main']);

myApp.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            controller: 'IndexController',
            templateUrl: 'Scheduler/MonthScheduler'
        })
        .when('/employees', {
            controller: 'EmployeeListController',
            templateUrl: 'Employee/EmployeeList'
        })
        .when('/employees/add', {
            controller: 'AddEmployeeController',
            templateUrl: 'Employee/AddEmployee'
        })
        .when('/scheduler/worksummary/:year/:month', {
            controller: 'SchedulerWorkSummary',
            templateUrl: 'Scheduler/WorkSummary'
        })
        .when('/scheduler/:year/:month', {
            controller: 'IndexController',
            templateUrl: 'Scheduler/MonthScheduler'
        })
        .when('/employees/:employeeId/edit', {
            controller: 'EditEmployeeController',
            templateUrl: 'Employee/EditEmployee'
        })
        .otherwise({
            redirectTo: '/'
        });
});

myApp.filter('range', function () {
    return function (n) {
        var result = [];

        for (var i = 0; i < n; i++) {
            result.push(i);
        }

        return result;
    };
});

myApp.filter('floor', function () {
    return function (n) {
        return Math.floor(n);
    };
});

myApp.filter('time', function () {
    return function (time) {
        return time.substring(0, 5);
    };
});

myApp.filter('min2h', function() {
    return function(minutes) {
        if (minutes !== "") {
            var min = parseInt(minutes);
            return "" + ((min / 60) >> 0) + "h " + Math.floor(min % 60) + "m";
        }
        return "-";
    };
});

myApp.controller('SchedulerWorkSummary', function ($scope, $http, $location, $routeParams, dialogs) {
    $scope.year = $routeParams.year;
    $scope.month = $routeParams.month;

    $scope.load = function() {
        return $http.get('/api/scheduler/' + $scope.year + '/' + $scope.month + '/worksummary')
            .success(function(data) {
                $scope.data = data;
            });
    };

    $scope.save = function() {
        $http.post('/api/scheduler/' + $scope.year + '/' + $scope.month + '/worksummary', $scope.data)
            .then(function() {
                $scope.load()
                    .then(function() {
                        $location.path('/scheduler');
                    });
            });
    };

    $scope.cancel = function() {
        $location.path('/scheduler/' + $scope.year + '/' + $scope.month);
    };

    $scope.lock = function() {
        dialogs.confirm('Blokowanie miesiąca', 'Uniemożliwi to wprowadzanie zmian.', 's')
            .result
            .then(function() {
                $http.post('/api/scheduler/' + $scope.year + '/' + $scope.month + '/lock')
                    .then($scope.load);
            });
    };

    $scope.workForNextMonth = function(employee) {
        return employee.MinutesToWork - employee.WorkFromPreviousMonth;
    };

    $scope.load();
});

myApp.controller('EmployeeListController', function ($scope, $http, $location) {
    $scope.startDateOpen = function($event) {

    };

    $scope.init = function() {
        $http.get('/api/employee')
            .success(function (data) {
                $scope.data = data;
            });
    };

    $scope.edit = function(employeeId) {
        $location.path('/employees/' + employeeId + '/edit');
    };

    $scope.init();
});

myApp.controller('EditEmployeeController', function ($scope, $http, $routeParams, $location) {
    $scope.employeeId = $routeParams.employeeId;

    $scope.init = function () {
        $http.get('/api/employee/' + $scope.employeeId)
            .success(function (data) {
                $scope.data = data;
            });
    }

    $scope.EmploymentTypes = [
        {
            id: 0,
            title: "Umowa o pracę (zmianowa)"
        },
        {
            id: 1,
            title: "Umowa o pracę (dzienna)"
        },
        {
            id: 2,
            title: "Działalność"
        }
    ];

    $scope.choseEndDateOpened = false;
    $scope.choseEndDate = function() {
        $scope.choseEndDateOpened = true;
    };

    $scope.choseStartDateOpened = false;
    $scope.choseStartDate = function () {
        $scope.choseStartDateOpened = true;
    };

    $scope.save = function() {
        $http.post('/api/employee/' + $scope.employeeId, $scope.data)
            .success(function(data) {
                $location.path('/employees');
            });
    }

    $scope.init();
});

myApp.controller('AddEmployeeController', function ($scope, $http, $routeParams, $location) {
    $scope.employeeId = $routeParams.employeeId;

    $scope.init = function () {
    }

    $scope.EmploymentTypes = [
        {
            id: 0,
            title: "Umowa o pracę (zmianowa)"
        },
        {
            id: 1,
            title: "Umowa o pracę (dzienna)"
        },
        {
            id: 2,
            title: "Działalność"
        }
    ];

    $scope.choseEndDateOpened = false;
    $scope.choseEndDate = function () {
        $scope.choseEndDateOpened = true;
    };

    $scope.choseStartDateOpened = false;
    $scope.choseStartDate = function () {
        $scope.choseStartDateOpened = true;
    };

    $scope.save = function () {
        $http.post('/api/employee/add', $scope.data)
            .success(function (data) {
                $location.path('/employees');
            });
    }

    $scope.init();
});

myApp.controller('ShowEmployeeController', function ($scope, $modalInstance, $http, data) {
    $scope.init = function (employeeId) {
        $http.get('/api/employee/' + employeeId + '/summary')
            .success(function (data) {
                $scope.data = data;
            });
    };

    $scope.cancel = function () {
        $modalInstance.close();
    };

    $scope.init(data.employeeId);
});

myApp.controller('MonthSummaryController', function ($scope, $modalInstance, $http, data) {
    $scope.init = function(year, month) {
        $scope.uri = '/api/month/' + month + '/' + year;

        $http.get($scope.uri + '/summary')
            .success(function(data) {
                $scope.data = data;
            })
            .then(function() {
                $http.get($scope.uri + '/employeesummary')
                    .success(function(data) {
                        $scope.employees = data;
                    });
            });
    };

    $scope.cancel = function () {
        $modalInstance.close();
    };

    $scope.init(data.year, data.month);
});

myApp.controller('EditDayDetailsController', function ($scope, $modalInstance, $http, data) {
    $scope.hstep = 1;
    $scope.mstep = 5;

    $scope.ok = function () {
        $http.post($scope.uri, $scope.data)
            .success(function (data) {
                $scope.data = data;
                return $modalInstance.close();
            });
    };

    $scope.cancel = function () {
        $modalInstance.close();
    };

    $scope.init = function (year, month, day, employeeId) {
        $scope.uri = '/api/scheduler/' + year + '/' + month + '/' + day + '/employee/' + employeeId + '/dayinfo';
        $http.get($scope.uri)
            .success(function (data) {
                $scope.data = data;
            });
    };

    $scope.setEnd = function (hour, minute) {
        var d = new Date();
        d.setHours(hour);
        d.setMinutes(minute);
        $scope.data.End = d;
    };

    $scope.setStart = function (hour, minute) {
        var d = new Date();
        d.setHours(hour);
        d.setMinutes(minute);
        $scope.data.Start = d;
    };

    $scope.init(data.year, data.month, data.day, data.employeeId);
});

myApp.controller('IndexController', ['$scope', '$http', '$modal', '$routeParams', '$location', 'dialogs', IndexController]);

function IndexController($scope, $http, $modal, $routeParams, $location, dialogs) {

    $scope.isBusy = true;
    if ($routeParams.year != undefined && $routeParams.month != undefined) {
        $scope.scheduler = {
            Year: $routeParams.year,
            Month: $routeParams.month
        };
    }

    $scope.init = function () {
        $scope
            .load();
    };

    $scope.load = function () {
        var uri = '/api/scheduler';
        if ($scope.scheduler != undefined) {
            var year = $scope.scheduler.Year;
            var month = $scope.scheduler.Month
            uri = uri + '/' + year + '/' + month;
        }
        return $http
            .get(uri)
            .success(function (data) {
                $scope.isBusy = false;
                $scope.scheduler = data;
            });
    };

    $scope.nextMonth = function () {
        var year = $scope.scheduler.Year;
        var month = $scope.scheduler.Month + 1;
        if (month > 12) {
            month = 1;
            year = year + 1;
        }

        $location.path('/scheduler/' + year + '/' + month);
    };

    $scope.prevMonth = function () {
        var year = $scope.scheduler.Year;
        var month = $scope.scheduler.Month - 1;
        if (month < 1) {
            month = 12;
            year = year - 1;
        }
        $location.path('/scheduler/' + year + '/' + month);
    };

    $scope.setDayDetail = function (employeeDay, employeeSchedule) {
        if ($scope.defaultClickAction === "LOCK") {
            return $scope.fixDay(employeeDay, employeeSchedule);
        }

        if ($scope.defaultClickAction === "LEAVE") {
            return $scope.setLeave(employeeDay, employeeSchedule);
        }

        $scope.isBusy = true;

        var employeeId = employeeSchedule.EmployeeId;
        var uri = '/api/scheduler/' + $scope.scheduler.Year + '/' + $scope.scheduler.Month + '/' + employeeDay.Day + '/employee/' + employeeId + '/';
        var data = {
            DayType: $scope.defaultClickAction,
            Start: employeeDay.Start,
            End: employeeDay.End
        };

        if (data.DayType == employeeDay.DayType) {
            data.DayType = 0;
        }

        return $http.post(uri, data)
            .success(function (data) {
                $scope.load();
            });
    };

    $scope.defaultClickAction = 0;

    $scope.autoPlan = function () {

        $scope.isBusy = true;
        var uri = '/api/scheduler/' + $scope.scheduler.Year + '/' + $scope.scheduler.Month + '/autoplan';

        $http.post(uri)
            .success(function () {
                $scope.load();
            });
    };

    $scope.cleanMonth = function () {
        dialogs.confirm('Czyszczenie miesiąca', 'Wszystkie niezablokowane informacje zostaną usunięte.', 's')
            .result
            .then(function () {
                $scope.isBusy = true;
                var uri = '/api/scheduler/' + $scope.scheduler.Year + '/' + $scope.scheduler.Month + '/clean';
                $http.post(uri)
                    .success(function () {
                        $scope.load();
                    });
            });
    };

    $scope.fixDay = function (employeeDay, employeeSchedule) {
        var employeeId = employeeSchedule.EmployeeId;
        var uri = '/api/scheduler/' + $scope.scheduler.Year + '/' + $scope.scheduler.Month + '/' + employeeDay.Day + '/employee/' + employeeId + '/fixday';
        var data = {
            MakeReadOnly: !employeeDay.IsFixed
        };

        $scope.isBusy = true;
        $http.post(uri, data)
            .success(function (data) {
                $scope.load();
            });
    };

    $scope.setLeave = function(employeeDay, employeeSchedule) {
        var employeeId = employeeSchedule.EmployeeId;
        var uri = '/api/scheduler/' + $scope.scheduler.Year + '/' + $scope.scheduler.Month + '/' + employeeDay.Day + '/employee/' + employeeId + '/leave';
        var data = {
            HasLeave: !employeeDay.HasLeave
        };

        $scope.isBusy = true;
        $http.post(uri, data)
            .success(function (data) {
                $scope.load();
            });
    }

    $scope.editDayDetails = function (employeeDay, employeeSchedule) {
        var modalInstance = $modal.open({
            templateUrl: '/Home/EditDayDetails',
            controller: 'EditDayDetailsController',
            size: 'md',
            resolve: {
                data: function () {
                    return {
                        year: $scope.scheduler.Year,
                        month: $scope.scheduler.Month,
                        day: employeeDay.Day,
                        employeeId: employeeSchedule.EmployeeId
                    }
                }
            }
        });

        return modalInstance.result
            .then(function (data) {
                $scope.isBusy = true;
                return $scope.load();
            });
    };

    $scope.dayClick = function (employeeDay, employeeSchedule) {
        if ($scope.defaultClickAction == 5) {
            return $scope.editDayDetails(employeeDay, employeeSchedule);
        }

        return $scope.setDayDetail(employeeDay, employeeSchedule);
    };

    $scope.showEmployee = function (employeeSchedule) {
        var modalInstance = $modal.open({
            templateUrl: '/Home/ShowEmployee',
            controller: 'ShowEmployeeController',
            size: 'md',
            resolve: {
                data: function () {
                    return {
                        employeeId: employeeSchedule.EmployeeId
                    }
                }
            }
        });
    };

    $scope.monthSummary = function() {
        var modalInstance = $modal.open({
            templateUrl: '/Home/MonthSummary',
            controller: 'MonthSummaryController',
            size: 'md',
            resolve: {
                data: function () {
                    return {
                        year: $scope.scheduler.Year,
                        month: $scope.scheduler.Month
                    }
                }
            }
        });
    };

    $scope.isBankHoliday = function (employeeDay) {

        if (employeeDay.__isBankHoliday !== undefined) {
            return employeeDay.__isBankHoliday;
        }
        employeeDay.__isBankHoliday = false;

        $scope.scheduler.Days.forEach(function(element) {
            if (element.Day == employeeDay.Day) {
                employeeDay.__isBankHoliday = element.IsHoliday;
                return;
            }
        });
    };

    $scope.isSaturday = function(employeeDay) {
        if (employeeDay.__isSaturday !== undefined) {
            return employeeDay.__isSaturday;
        }
        employeeDay.__isSaturday = false;

        $scope.scheduler.Days.forEach(function (element) {
            if (element.Day == employeeDay.Day) {
                employeeDay.__isSaturday = element.DayOfWeek == 6;
                return;
            }
        });
    };

    $scope.init();
}

﻿using System.Web.Mvc;

namespace DutyRoster.Facing.Controllers
{
    public sealed class SchedulerController : Controller
    {
        public ActionResult MonthScheduler()
        {
            return View();
        }

        public ActionResult WorkSummary()
        {
            return View();
        }
    }
}
﻿using System.Web.Mvc;

namespace DutyRoster.Facing.Controllers
{
    public sealed class EmployeeController : Controller
    {
        public ActionResult EmployeeList()
        {
            return View();
        }

        public ActionResult AddEmployee()
        {
            return View();
        }

        public ActionResult EditEmployee()
        {
            return View();
        }
    }
}
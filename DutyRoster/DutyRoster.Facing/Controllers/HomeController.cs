﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DutyRoster.Facing.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EditDayDetails()
        {
            return View();
        }

        public ActionResult ShowEmployee()
        {
            return View();
        }

        public ActionResult MonthSummary()
        {
            return View();
        }
    }
}